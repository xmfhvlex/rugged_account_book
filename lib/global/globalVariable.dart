import 'dart:convert'; //to convert json to maps and vice versa
import 'dart:io';
import 'dart:async';
import 'dart:math' show Random;
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart'; //add path provider dart plugin on pubspec.yaml file
import 'package:random_string/random_string.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:path/path.dart';
import 'package:simple_permissions/simple_permissions.dart';
import 'package:flutter_file_manager/flutter_file_manager.dart';
import 'package:spreadsheet_decoder/spreadsheet_decoder.dart';

// class EventListener{
//   EventMessage _
// }

class EventMessage{
  Map<Function, Function> mapCallback = Map<Function, Function>();
  List<Function> lstUnregister = List<Function>();
  int num = 500;

  void send(dynamic data){
    for(var callback in lstUnregister)
      mapCallback.remove(callback);
    lstUnregister.clear();

    mapCallback.forEach((k, v){ v(data); });

    for(var callback in lstUnregister)
      mapCallback.remove(callback);
    lstUnregister.clear();
  }

  void register(Function(dynamic) callback){
    for(var callback in lstUnregister)
      mapCallback.remove(callback);
    lstUnregister.clear();

    if(mapCallback.containsKey(callback) == false)
      mapCallback[callback] = callback;
  }

  void unregister(Function(dynamic) callback){
    //_mapCallback.clear();
    // if(_mapCallback.containsKey(callback))
    //   _mapCallback.remove(callback);
    
    lstUnregister.add(callback);
  }

  int count(){
    return mapCallback.length;
  }

  void clear(){
    mapCallback.clear();
  }
}

class GlobalVariable{
  static GlobalVariable _instance;

  DateTime pageDateTime = DateTime.now();

  Map<String, FocusNode> _mFocusNode;
  FocusNode addFocusNode(String name){
    FocusNode focusNode = new FocusNode();

    if(_mFocusNode.containsKey(name) == false){
      _mFocusNode[name] = focusNode;
    }
    return focusNode;
  }
  FocusNode getFocusNode(String name){
    if(_mFocusNode.containsKey(name) == false){
      return null;
    }
    return _mFocusNode[name];
  }
  void deleteFocusNode(String name){
    if(_mFocusNode.containsKey(name) == true){
      _mFocusNode[name].dispose();
      _mFocusNode.remove(name);
    }
  }

 Map<String, StreamController> _mStream;
  StreamController addStream(String name){
    StreamController stream = new StreamController.broadcast();
    if(_mStream.containsKey(name) == false){
      _mStream[name] = stream;
    }
    return stream;
  }
  StreamController getStream(String name){
    if(_mStream.containsKey(name) == false){
      return null;
    }
    return _mStream[name];
  }
  void deleteStream(String name){
    if(_mStream.containsKey(name) == true){
      _mStream[name].close();
      _mStream.remove(name);
    }
  }

  Map<String, EventMessage> _mEvent;
  EventMessage createEvent(String name){
    if(_mEvent.containsKey(name) == false)
      _mEvent[name] = new EventMessage();
    return _mEvent[name];
  }
  EventMessage getEvent(String name){
    if(_mEvent.containsKey(name) == false){
      return null;
    }
    return _mEvent[name];
  }
  void deleteEvent(String name){
    if(_mEvent.containsKey(name) == true){
      _mEvent.remove(name);
    }
  }

  static GlobalVariable inst(){
    if(GlobalVariable._instance == null){
      GlobalVariable._instance = GlobalVariable.internal();
      GlobalVariable._instance._mFocusNode = new Map<String, FocusNode>();
      GlobalVariable._instance._mStream = new Map<String, StreamController>();
      GlobalVariable._instance._mEvent = new Map<String, EventMessage>();
    }
    return GlobalVariable._instance;
  }

  GlobalVariable.internal();
}