import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter_test_00/custum_wiget_tool.dart';
import 'package:flutter_test_00/user_data.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:page_indicator/page_indicator.dart';
import 'chart_item.dart';
import 'cost.dart';

class WMonthChart extends StatefulWidget {
  final int year;
  final int month;

  WMonthChart({Key key, this.year, this.month}) : super(key: key);
  @override StatefulElement createElement() {
    return super.createElement();
  }
  @override WMonthChartState createState() => WMonthChartState();
}

class WMonthChartState extends State<WMonthChart> {
  PageController pageController;

  @override void initState() {
    super.initState();
    pageController = new PageController();
  }

  @override void didUpdateWidget(WMonthChart oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override void dispose() {
    pageController.dispose();
    super.dispose();
  }
  
  @override Widget build(BuildContext context){
    final screenWidth = MediaQuery.of(context).size.width;

    List lstChart = <Widget>[
      WMonthlyCategoryChart(year: widget.year, month: widget.month,),
      WDailyCategoryChart(year: widget.year, month: widget.month,),
    ];

    double indicatorSpace = 5;
    double pageViewIndicatorWidth = screenWidth / lstChart.length - indicatorSpace;

    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(const  Radius.circular(10.0)),
        color: Color.fromARGB(255, 45, 45, 45),
//         gradient: LinearGradient(
//           begin: Alignment.topCenter,
//           end: Alignment.bottomCenter,
//           colors: [
// //            Color.fromARGB(255, 200, 200, 200),
//             Color.fromARGB(255, 45, 45, 45),
//             Color.fromARGB(255, 45, 45, 45),
//           ],
//         ),
      ),   
      child: PageIndicatorContainer(
        child: PageView(
          controller: pageController,
          children: lstChart
        ),
        align: IndicatorAlign.bottom,
        length: lstChart.length,
        indicatorSpace: indicatorSpace,
        indicatorColor: Colors.white10,
        indicatorSelectorColor: Colors.white,
        padding: const EdgeInsets.only(bottom: 0),
        shape: IndicatorShape.roundRectangleShape(size: Size(pageViewIndicatorWidth, 3),cornerSize: Size.square(3)),
      ),
    );
  }
}

class WMonthlyCategoryChart extends StatefulWidget {
  final int year;
  final int month;

  WMonthlyCategoryChart({Key key, this.year, this.month}) : super(key: key);

  @override StatefulElement createElement() {
    return super.createElement();
  }

  @override State createState() => WMonthlyCategoryChartState();
}

class WMonthlyCategoryChartState extends State<WMonthlyCategoryChart> {
// with AutomaticKeepAliveClientMixin<WMonthlyCategoryChart>
//  @override bool get wantKeepAlive => true; 

  @override Widget build(BuildContext context) {
    return FutureBuilder(
      future: UserData.inst().queryAccount(
        """SELECT category, cost FROM AccountData 
          WHERE 
            year == ${widget.year} AND 
            month == ${widget.month}
        """), // a previously-obtained Future<String> or null
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
          case ConnectionState.active:
            return Container();
          case ConnectionState.waiting:
            return SpinKitCircle(
              color: Colors.white,
              size: 100.0,
            );
          case ConnectionState.done:
            if (snapshot.hasError)
                return Text('Error: ${snapshot.error}');
            var lstAccount = snapshot.data;

            //Add All Category
            var categoryCost = Map<String, double>();
            UserData.inst().categoryData.forEach((k, v) {
              if(k != "전체")
                categoryCost[k] = 0;
            });

            int sumCost = 0;
            for (var item in lstAccount){
              var category = UserData.inst().getCategoryData(item.category);
              categoryCost[category["category"]] += item.cost.toDouble();
              sumCost += item.cost;
            }

            //Remove 0 Valued Category
            //categoryCost.removeWhere((k, v) {return v == 0.0; });

            int leftPadding = 0;
            var lstWidget = List<Widget>();
            categoryCost.forEach((k, v){
              lstWidget.add(WChartItem( data: <String, dynamic>{ "category": k, "cost": v.toInt(), "leftPadding": leftPadding, "sumCost": sumCost } ));
              leftPadding += v.toInt();
            });

            lstWidget.add(
              Container(
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.white54,
                  ),
                  color: Color.fromARGB(100, 20, 20, 20),
                  borderRadius: BorderRadius.all(const Radius.circular(45.0)),
                ),
                height: 30,
                child: Center(
                  child: Text(gCostFormatter.format(sumCost) + " ₩"),
                ),
              )
            );

            return Container(
          //    padding: const EdgeInsets.only(top: 0, bottom: 10),
              child: Column(
                children: <Widget>[
                  Expanded(
                    flex: 10,
                    child: Column(
                      children: lstWidget,
                    ),
                  ),
                ],
              ),
            );
        }
        return Container();
      },
    );
  }
}



class WDailyCategoryChart extends StatelessWidget{
  final int year;
  final int month;

  WDailyCategoryChart({Key key, this.year, this.month}) : super(key: key);

  @override Widget build(BuildContext context) {
    final availableWidth = MediaQuery.of(context).size.width;

    return FutureBuilder(
      future: UserData.inst().queryAccount(
        """SELECT category, cost, day FROM AccountData 
          WHERE 
            year == $year AND 
            month == $month
          ORDER BY 
            year, 
            month, 
            day
        """), // a previously-obtained Future<String> or null
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
          case ConnectionState.active:
            return Container();
          case ConnectionState.waiting:
            return SpinKitCircle(
              color: Colors.white,
              size: 100.0,
            );
          case ConnectionState.done:
            if (snapshot.hasError)
                return Text('Error: ${snapshot.error}');
            List lstAccount = snapshot.data;

            List lstWidget = new List<Widget>();

            var categoryCost = Map<String, double>();
            UserData.inst().categoryData.forEach((k, v) {
              if(k != "전체")
                categoryCost[k] = 0;
            });

            double sumMonthCost = 0;
            for (var item in lstAccount){
              sumMonthCost += item.cost;
            }

            double largetstDayCost = 0;
            double pvtCost = 0;
            int prvDay = 1;
            for(var item in lstAccount){
              if(item.day != prvDay){
                prvDay = item.day;
                pvtCost = 0;
              }
              pvtCost += item.cost;
              largetstDayCost = (largetstDayCost < pvtCost) ? pvtCost : largetstDayCost;
            }

            double prevCost = 0;
            for(int i=1 ; i<32 ; i++){
              //Add All Category
              var tempCategoryCost = new Map<String, double>.from(categoryCost);

              double sumDayCost = 0;
              for(var item in lstAccount){
                if(item.day == i){
                  var category = UserData.inst().getCategoryData(item.category);
                  tempCategoryCost[category["category"]] += item.cost;
                  sumDayCost += item.cost;
                }
              }

              tempCategoryCost.removeWhere((k, v){ return v == 0; });

              lstWidget.add(WGraphChartItem(date: DateTime(year, month, i), sumMonthCost: sumMonthCost, sumDayCost: sumDayCost, prevCost: prevCost, largetstDayCost: largetstDayCost, categoryCost: tempCategoryCost,));
              lstWidget.add(Expanded(flex: 1, child: Container(color: Colors.transparent,)));
              prevCost += sumDayCost;
            }

            return Container(
              padding: const EdgeInsets.only(top: 0, bottom: 5),
              child: Row(
                children: lstWidget,
              ),
            );
        }
        return Container();
      },
    );
  }
}

  // child: Stack(
  //       children :[ 
  //         Positioned(
  //           left: 300,
  //           top: 100,
  //           child: Container(
  //             width: 200,
  //             height: 200,
  //             color: Colors.red,
  //           ),
  //         ),
  //       ],








  
class WYearChart extends StatefulWidget {
  final int year;

  WYearChart({Key key, this.year}) : super(key: key);
  @override StatefulElement createElement() {
    return super.createElement();
  }
  @override WYearChartState createState() => WYearChartState();
}

class WYearChartState extends State<WYearChart> {
  PageController pageController;

  @override void initState() {
    super.initState();
    pageController = new PageController();
  }

  @override void didUpdateWidget(WYearChart oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override void dispose() {
    pageController.dispose();
    super.dispose();
  }
  
  @override Widget build(BuildContext context){
    final screenWidth = MediaQuery.of(context).size.width;

    List lstChart = <Widget>[
      WYearCategoryChart(year: widget.year),
      WYearCategoryChart(year: widget.year),
      WYearCategoryChart(year: widget.year),
    //  WDailyCategoryChart(year: widget.year, month: widget.month,),
    ];

    double indicatorSpace = 5;
    double pageViewIndicatorWidth = screenWidth / lstChart.length - indicatorSpace;

    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(const  Radius.circular(10.0)),
        color: Color.fromARGB(255, 45, 45, 45),
      ),   
      child: PageIndicatorContainer(
        child: PageView(
          controller: pageController,
          children: lstChart
        ),
        align: IndicatorAlign.bottom,
        length: lstChart.length,
        indicatorSpace: indicatorSpace,
        indicatorColor: Colors.white10,
        indicatorSelectorColor: Colors.white,
        padding: const EdgeInsets.only(bottom: 0),
        shape: IndicatorShape.roundRectangleShape(size: Size(pageViewIndicatorWidth, 3),cornerSize: Size.square(3)),
      ),
    );
  }
}

class WYearCategoryChart extends StatefulWidget {
  final int year;
  final int month;

  WYearCategoryChart({Key key, this.year, this.month}) : super(key: key);

  @override StatefulElement createElement() {
    return super.createElement();
  }

  @override State createState() => WYearCategoryChartState();
}

class WYearCategoryChartState extends State<WYearCategoryChart> {
  @override Widget build(BuildContext context) {
    return FutureBuilder(
      future: UserData.inst().queryAccount(
        """SELECT category, cost FROM AccountData 
          WHERE 
            year == ${widget.year}
        """), // a previously-obtained Future<String> or null
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
          case ConnectionState.active:
            return Container();
          case ConnectionState.waiting:
            return SpinKitCircle(
              color: Colors.white,
              size: 100.0,
            );
          case ConnectionState.done:
            if (snapshot.hasError)
                return Text('Error: ${snapshot.error}');
            var lstAccount = snapshot.data;

            //Add All Category
            var categoryCost = Map<String, double>();
            UserData.inst().categoryData.forEach((k, v) {
              if(k != "전체")
                categoryCost[k] = 0;
            });

            int sumCost = 0;
            for (var item in lstAccount){
              var category = UserData.inst().getCategoryData(item.category);
              categoryCost[category["category"]] += item.cost.toDouble();
              sumCost += item.cost;
            }

            //Remove 0 Valued Category
            //categoryCost.removeWhere((k, v) {return v == 0.0; });

            int leftPadding = 0;
            var lstWidget = List<Widget>();
            categoryCost.forEach((k, v){
              lstWidget.add(WChartItem( data: <String, dynamic>{ "category": k, "cost": v.toInt(), "leftPadding": leftPadding, "sumCost": sumCost } ));
              leftPadding += v.toInt();
            });

            lstWidget.add(
              Container(
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.white54,
                  ),
                  color: Color.fromARGB(100, 20, 20, 20),
                  borderRadius: BorderRadius.all(const Radius.circular(45.0)),
                ),
                height: 30,
                child: Center(
                  child: Text(gCostFormatter.format(sumCost) + " ₩"),
                ),
              )
            );

            return Container(
          //    padding: const EdgeInsets.only(top: 0, bottom: 10),
              child: Column(
                children: <Widget>[
                  Expanded(
                    flex: 10,
                    child: Column(
                      children: lstWidget,
                    ),
                  ),
                ],
              ),
            );
        }
        return Container();
      },
    );
  }
}
