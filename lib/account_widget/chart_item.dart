import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_test_00/user_data.dart';
import 'package:flutter_test_00/custum_wiget_tool.dart';
import 'category.dart';
import 'cost.dart';

class WChartItem extends StatefulWidget{
  String mCategory = "";
  int    mCost = 0;
  int   mSumCost;
  int   mLeftPadding;
  double mHeight = 30;

  WChartItem({dynamic data}){
    mCategory = data["category"];
    mCost = data["cost"];
    mSumCost = data["sumCost"];
    mLeftPadding = data["leftPadding"];
  }  
  @override StatefulElement createElement() {
    return super.createElement();
  }
  @override WChartItemState createState() => WChartItemState();
}

class WChartItemState extends State<WChartItem> with TickerProviderStateMixin{

  AnimationController _animationController;
  Animation _animation;

  @override void initState() {
    _animationController = AnimationController(duration: Duration(milliseconds: 1000), vsync: this);
    _animation = Tween<double>(begin: 0, end: 1).animate(CurvedAnimation(parent: _animationController, curve: Curves.ease));
    _animation.addListener((){ setState((){}); });
    _animationController.forward();

    super.initState();
  } 

  @override void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override Widget build(BuildContext context) {
    double width = 150;
    
    
    return Container(
      decoration: BoxDecoration(
        color: Color.fromARGB(50, 0, 0, 0),
        borderRadius: BorderRadius.all(const Radius.circular(45.0)),
      ), 
      height: widget.mHeight,
     // width: 350,
      padding: const EdgeInsets.only(bottom:4),
      margin: const EdgeInsets.only(left:10.0, right: 10.0),
      child: Row(
        children: <Widget>[ 
        //  WSpacer(width:5),
          WCategory(mCategory: widget.mCategory, mIsChart: true,),
          Expanded(
            flex: 25,
            child: Container(
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.white54,
                ),
                color: Color.fromARGB(200, 0, 0, 0),
                borderRadius: BorderRadius.only(topRight: const Radius.circular(5.0), bottomRight: const Radius.circular(5.0)),
              ),
              child: Stack(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Expanded(
                        flex: (widget.mLeftPadding * _animation.value).toInt(),
                        child: Container(),
                      ),
                      Expanded(
                        flex: (widget.mCost * _animation.value).toInt(),
                        child: Container(
                          decoration: BoxDecoration(
                            color: UserData.inst().getCategoryColor(widget.mCategory),
                            border: Border.all(
                              color: Colors.white70,
                            ),
                            borderRadius: (widget.mLeftPadding!=0) ? BorderRadius.all(const Radius.circular(5.0)) :
                                                             BorderRadius.only(topRight: const Radius.circular(5.0), bottomRight: const Radius.circular(5.0)),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: (widget.mSumCost - (widget.mCost * _animation.value) - widget.mLeftPadding).toInt(),
                        child: Container(),
                      ),
                    ],
                  ),
                  Positioned(
                    child: Align(
                      alignment: Alignment(0, 0),
                      child: BorderedText(
                        strokeWidth: 1,
                        strokeColor: Colors.black,
                        child: Text(gPercentFormatter.format(widget.mSumCost!=0 ? (widget.mCost * _animation.value)/widget.mSumCost* 100 : 0) + " %",
                          style: TextStyle(
                            fontSize: 10, 
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),

          WSpacer(width:3, color:Colors.transparent),

          WCost(cost: widget.mCost, width: 80, height: widget.mHeight,),
        ],
      ),
    );
  }
}

class WGraphChartItem extends StatelessWidget{
  final DateTime date;
  final double sumMonthCost;
  final double sumDayCost;
  final double prevCost;
  final double largetstDayCost;
  final Map<String, dynamic> categoryCost;

  WGraphChartItem({this.date, this.sumMonthCost, this.sumDayCost, this.prevCost, this.largetstDayCost, this.categoryCost});

  @override Widget build(BuildContext context) {
    
    final availableWidth = MediaQuery.of(context).size.width;

    List lstWidget = new List<Widget>();


  //  lstWidget.add(
  //     Expanded(
  //       flex: (sumMonthCost - sumDayCost - prevCost).toInt(),
  //       child: Container(),
  //     )
  //   );

    lstWidget.add(
      Expanded(
        flex: (largetstDayCost - sumDayCost).toInt(),
        child: Container(),
      )
    );
    

    int idx = 0;
    categoryCost.forEach((k, v){
      lstWidget.add(
        Expanded(
          flex: v.toInt(),
          child: GestureDetector(
            onLongPressStart: (details){ 

              print(details.globalPosition);
              var scaffold = Scaffold.of(context, nullOk: true);
              scaffold?.showSnackBar(
                SnackBar(
                //  duration: Duration(milliseconds: 300),
                  content: Row(
                    children: <Widget>[
                      Icon(
                        Icons.money_off,
                        size: 20,
                      ),
                      SizedBox(width: 8),
                      Expanded(
                        child: Text(k.toString() + " - " + gCostFormatter.format(v) + " ₩")
                      )
                    ],
                  )
                )
              );


              // OverlayState overlayState = Overlay.of(context);
              // OverlayEntry overlayEntry = OverlayEntry(
              //   builder: (context) => Positioned(
              //     left: details.globalPosition.dx,
              //     top: details.globalPosition.dy,
              //     child: CircleAvatar(
              //       radius: 10.0,
              //       backgroundColor: Colors.red,
              //       child: Text("1"),
              //     ),
              //   ));

              // overlayState.insert(overlayEntry);

              // await Future.delayed(Duration(seconds: 2));

              // overlayEntry.remove();

              showOverlay(context, details.globalPosition);
            },
            onLongPressEnd: (details){
              var scaffold = Scaffold.of(context, nullOk: true);
              scaffold?.hideCurrentSnackBar();
            },
            onLongPress: (){},
            child: Container(
              decoration: BoxDecoration(
              color: UserData.inst().getCategoryColor(k),
            //  borderRadius: BorderRadius.all(const Radius.circular(3.0)),
              borderRadius: radius(idx: idx, length: categoryCost.length),
            ), 
            //  margin: const EdgeInsets.only(left:1.0, right: 1.0),
              width: availableWidth/31-4,
            ),
          ),
        ),
      );
      idx++;
    });

    // lstWidget.add(
    //   Expanded(
    //     flex: prevCost.toInt(),
    //     child: Container(),
    //   )
    // );
    Color txtColor;
    switch(date.weekday){
      case 6:
        txtColor = Colors.cyan;
        break;
      case 7:
        txtColor = Colors.pink;
        break;
      default:
        txtColor = Colors.white;
        break;

    }
    return Container(
      color: (sumDayCost>0) ? Colors.black38 : Colors.black12,
      child: Column(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: (sumDayCost>0) ? Column( children: lstWidget, ) :
              Container(width: availableWidth/31,),
          ),
          Text(gDayFormatter.format(date.day),
            style: TextStyle(
              fontSize: 8,
              color: txtColor,
            ),
          ),
        ],
      )
    );
  }

  BorderRadius radius({int idx, int length}){
    const double rad = 4;

//DateFormat('EEEE').format(date);
 //   DateTime
return BorderRadius.all(const Radius.circular(rad));  
    if(length == 1){
      return BorderRadius.all(const Radius.circular(rad));  
    }
    else if(idx == 0){
      return BorderRadius.only(topLeft: const Radius.circular(rad), topRight: const Radius.circular(rad));  
    }
    else if(idx == length-1){
      return BorderRadius.only(bottomLeft: const Radius.circular(rad), bottomRight: const Radius.circular(rad));  
    }
    return null;
  }

  Future showOverlay(BuildContext context, Offset position) async {
    OverlayState overlayState = Overlay.of(context);
    OverlayEntry overlayEntry = OverlayEntry(
      builder: (context) => Positioned(
        left: position.dx,
        top: position.dy,
        child: Container(
          decoration: BoxDecoration(
            color: Color.fromARGB(50, 20, 20, 20),
            borderRadius: BorderRadius.all(const  Radius.circular(45.0)),
            border: Border.all(
              color: Colors.white24,
            ),
          ),
          //margin: const EdgeInsets.only(left: 135, right: 135, top:5, bottom:5),
          width: 100,
          height: 100,

          child: Material(
            child: InkWell(
              splashColor: Color.fromARGB(255, 255, 255, 255),
              radius: 200,
              borderRadius: BorderRadius.all(const Radius.circular(45.0)),
              onTap: () async{
                await Future.delayed(const Duration(milliseconds: 200));
              },
            )
          ),
        ),
      ));

    overlayState.insert(overlayEntry);

    await Future.delayed(Duration(seconds: 5));

    overlayEntry.remove();
  }
}
