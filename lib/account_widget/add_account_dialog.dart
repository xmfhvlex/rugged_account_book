import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_test_00/custum_wiget_tool.dart';
import 'category.dart';
import 'package:flutter_test_00/user_data.dart';

class WAccountAddDialog extends StatefulWidget {
  final DateTime date;
  final bool isSelectedDate;
  WAccountAddDialog({Key key, this.date, this.isSelectedDate=false}) : super(key: key){}

  @override WAccountAddDialogState createState(){
    return WAccountAddDialogState();
  }
}

class WAccountAddDialogState extends State<WAccountAddDialog> {
  DateTime _dateTime;
  String _category;
  var _ctrlMemoField = TextEditingController();
  var _ctrlCostField = TextEditingController();

  @override void initState() {
    super.initState();
    _category = "식사";
    _ctrlMemoField.text = "";
    _ctrlCostField.text = "";

    var nowTime = DateTime.now();
    if(widget.date == null){
      _dateTime = nowTime;  
    }
    else{
      if(widget.date.year == nowTime.year && widget.date.month == nowTime.month && widget.isSelectedDate==false)
        _dateTime = nowTime;
      else
        _dateTime = widget.date;
    }
  }

  Future selectDate(BuildContext context) async{
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: _dateTime,
      firstDate: DateTime(2000),
      lastDate: DateTime(2050),
      initialDatePickerMode: DatePickerMode.day,
    );

    if(picked != null && picked != _dateTime){
      _dateTime = picked;
    }

    setState(() {});
  }

  @override Widget build(BuildContext context) {
    return SimpleDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(const Radius.circular(30.0))
      ),
      backgroundColor: Color.fromARGB(255, 50, 50, 50),
      elevation: 10,
      title: Container(
        child: Align(
          child: Text("항목 추가",
            style: TextStyle(
              color: Colors.white,
            ),
          ),
        ),
      ),
      children: <Widget>[
          Container(
          margin: const EdgeInsets.only(left: 10.0, right: 10),
          decoration: BoxDecoration(
            color: Colors.white12,
            borderRadius: BorderRadius.all(const Radius.circular(10.0)),
          ),
          child: Row(
            children: [
              Container(
                margin: const EdgeInsets.only(left: 5.0),
                width: 60,
                child: Align(
                  child: BorderedText(
                    strokeColor: Colors.black,
                    strokeWidth: 1,
                    child: Text("날       짜 : ",
                      style: TextStyle(
                        fontSize: 12, 
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
              WSpacer(width: 15),
              CustumButton(
                iconSize: 15,
                width: 30,
                shape: CircleBorder(),
                backgroundColor: Color.fromARGB(30, 255, 255, 255),
                foregroundColor: Color.fromARGB(255, 255, 255, 255),
                iconData: Icons.remove,
                onPressed: () {
                  _dateTime = _dateTime.add(Duration(days: -1));
                  setState(() {});
                },
              ),
                Expanded(
                child: InkWell(
                  child: Container(
                    height: 50,
                    child: Align(
                      child: Text(gdate1Formatter.format(_dateTime)),       
                    ),
                  ),
                  onTap: (){
                    selectDate(context);
                  },
                ),
              ),
                CustumButton(
                iconSize: 15,
                width: 30,
                shape: CircleBorder(),
                backgroundColor: Color.fromARGB(30, 255, 255, 255),
                foregroundColor: Color.fromARGB(255, 255, 255, 255),
                iconData: Icons.add,
                onPressed: () {
                  _dateTime = _dateTime.add(Duration(days: 1));
                  setState(() {});
                },
              ),
              WSpacer(width: 15),
              // WSpacer(width: 10),              
              // CustumButton(
              //   shape: CircleBorder(),
              //   foregroundColor: Color.fromARGB(255, 0, 255, 0),
              //   backgroundColor: Color.fromARGB(255, 40, 40, 40),
              //   iconData: Icons.calendar_today,
              //   onPressed: () {
              //     selectDate(context);
              //   },
              // ),
            ]
          ),
        ),
          WSpacer(height: 10),
          Container(
          margin: const EdgeInsets.only(left: 10.0, right: 10),
          decoration: BoxDecoration(
            color: Colors.white12,
            borderRadius: BorderRadius.all(const Radius.circular(10.0)),
          ),
          child: Row(
            children: [
              Container(
                margin: const EdgeInsets.only(left: 5.0),
                width: 60,
                child: Align(
                  child: BorderedText(
                    strokeWidth: 1,
                    child: Text("카테고리 : ",
                      style: TextStyle(
                        fontSize: 12, 
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
                Expanded(
                child: Container(
                  margin: const EdgeInsets.only(left: 50.0, right: 50),
                  child: DropdownButtonHideUnderline(
                    child: ButtonTheme(
                      alignedDropdown: false,
                    // color: Colors.white,
                      child:  DropdownButton(
                        iconDisabledColor: Colors.black38,
                        isDense: true,
                        isExpanded: true,
                        iconSize: 30,
                        iconEnabledColor: Colors.white,
                        value: _category,
                        items: UserData.inst().createCategoryMenuItem(),
                        onChanged: (value){
                          _category = value;
                          setState(() {});
                        },
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
          
        WSpacer(height: 10),
          Container(
          margin: const EdgeInsets.only(left: 10.0, right: 10),
          decoration: BoxDecoration(
            color: Colors.white12,
            borderRadius: BorderRadius.all(const Radius.circular(10.0)),
          ),
          child: Row(
            children: [
              Container(
                margin: const EdgeInsets.only(left: 5.0),
                width: 60,
                child: Align(
                  child: BorderedText(
                    strokeWidth: 1,
                    child: Text("  메       모 : ",
                      style: TextStyle(
                        fontSize: 12, 
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  
                  child: TextFormField(
                    controller: _ctrlMemoField,
                    textInputAction: TextInputAction.done,
                    keyboardAppearance: Brightness.dark,
                    maxLength:35,
                    maxLines: 1,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 12,
                      color: Colors.white,
                    ),
                    decoration: InputDecoration(
                      counterStyle: TextStyle(
                        color: Colors.white30,
                      ),
                      hintText: "메모",
                    ),
                    onFieldSubmitted: (value){
                      setState(() {});
                    },
                    
                    onEditingComplete: null,
                    inputFormatters: [
                    //  LengthLimitingTextInputFormatter(1),
                    ],
                  ),
                ),
              ),
              WSpacer(width: 35),    
            ]
          ),
        ),
          WSpacer(height: 10),
          Container(
          margin: const EdgeInsets.only(left: 10.0, right: 10),
          decoration: BoxDecoration(
            color: Colors.white12,
            borderRadius: BorderRadius.all(const Radius.circular(10.0)),
          ),
          child: Row(
            children: [
              Container(
                margin: const EdgeInsets.only(left: 5.0),
                width: 60,
                child: Align(
                  child: BorderedText(
                    strokeWidth: 1,
                    child: Text("  금       액 : ",
                      style: TextStyle(
                        fontSize: 12, 
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                //  color: Colors.black38,
                  child: TextFormField(
                    
                    controller: _ctrlCostField,
                    textAlign: TextAlign.center,
                    keyboardType: TextInputType.number,
                    style: TextStyle(
                      fontSize: 10,
                      color: Colors.white,
                    ),
                    decoration: InputDecoration(
                      labelStyle: TextStyle(
                        color: Colors.white,
                      ),
                      hintText: "0",
                    ),
                    onEditingComplete: null,
                    onFieldSubmitted: (value){
          //            SystemChrome.setEnabledSystemUIOverlays([]);
                    },
                    
                    inputFormatters: [
                      CurrencyFormat(),
                    ],
                  ),
                ),
              ),
                WSpacer(width: 5),
              
              Container(
                width: 30,
                height: 30,
                decoration: BoxDecoration(
                  color: Color.fromARGB(100, 20, 20, 20),
                  borderRadius: BorderRadius.all(const Radius.circular(45.0)),
                ),
              //  color: Colors.white10,
                child: Align(
                  alignment: Alignment.center,
                  child: Text("(₩)",
                    style: TextStyle(
                      fontSize: 10, 
                      fontWeight: FontWeight.bold,
                      color: Colors.green,
                    ),
                  ),
                )
              ),
            ]
          ),
        ),
          WSpacer(height: 20),
          Container(
          child: Row(
            children: [
              WSpacer(width: 50),
              Container(
                decoration: BoxDecoration(
                  color: Colors.red,
                  borderRadius: BorderRadius.all(const Radius.circular(10.0)),
                ),
                child: SimpleDialogOption(
                  child: Text('취   소'),
                  onPressed: (){
                    Navigator.pop(context, null);
            //         SystemChrome.setEnabledSystemUIOverlays([]);
                  },
                ),
              ),
              Expanded(
                child: Container(
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  color: Colors.blue,
                  borderRadius: BorderRadius.all(const Radius.circular(10.0)),
                ),
                child: SimpleDialogOption(
                  child: Text('추   가'),
                  onPressed: (){
                  //  if(_ctrlCostField.text == "0")
                  //    return

                    Navigator.pop(context, <String, dynamic>{ 
                      "date": _dateTime, 
                      "category": _category,
                      "memo": _ctrlMemoField.text, 
                      "cost": _ctrlCostField.text != "" ? int.parse(_ctrlCostField.text.replaceAll(RegExp('[\,]'), '')) : 0,
                    });
            //         SystemChrome.setEnabledSystemUIOverlays([]);
                  },
                ),
              ),
              WSpacer(width: 50,),
            ]
          ),
        ),
      ],
    );
  }
}