import 'dart:async';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
//import 'package:googleapis/mirror/v1.dart';

import 'item_edit_bar.dart';
import 'category.dart';
import 'cost.dart';
import 'package:flutter_test_00/custum_wiget_tool.dart';
import 'package:flutter_test_00/user_data.dart';
import 'package:flutter_test_00/global/globalVariable.dart';
import 'package:flutter_test_00/dialog_widget/confirm_dialog.dart';

class WAccountItem extends StatefulWidget{
  dynamic data;
  WAccountItem({this.data});


  static void exitEditMode(){

  }

  @override State<StatefulWidget> createState() {
    return WAccountItemState(data:data);
  }
}

class WAccountItemState extends State<WAccountItem>  with TickerProviderStateMixin {
  int _key;
  String _category = "";
  String _prevCategory = "";

  String _memo = "";
  int    _cost = 0;
  bool  _isEditMode = false;
  double mHeight = 20;

  TextEditingController _cntrMemoField;
  TextEditingController _cntrCostField;

  OverlayEntry _overlayEntry;


  // AnimationController _animationController;
  // Animation _animation;

  WAccountItemState({dynamic data}){
    _key = data.key;
    _category = _prevCategory = data.category;
    _memo = data.memo;
    _cost = data.cost;
    _cntrMemoField = new TextEditingController(text: _memo);
    _cntrCostField = new TextEditingController(text: _cost.toString());
  }

  @override void initState() {
    // _animationController = AnimationController(duration: Duration(milliseconds: 1000), vsync: this);
    // _animation = Tween<double>(begin: 0, end: 1).animate(CurvedAnimation(parent: _animationController, curve: Curves.easeOutExpo));
    // _animation.addListener((){ setState((){}); });
    // _animationController.forward();
    super.initState();
  }

  @override void dispose() {
  //  _animationController.dispose();
    super.dispose();
  }

  @override Widget build(BuildContext context) {
    // return InkWell(
    //  // onLongPressStart: onLongPress,
    //  radius: 1500,
    //   onLongPress: onLongPress,
    //   child: _isEditMode ? editor() : item(),
    // );

    return _isEditMode ? editor() : item();
  }

  Widget item(){
    return Container(
      decoration: BoxDecoration(
      //  color: Color.fromARGB(50, 0, 0, 0),
        borderRadius: BorderRadius.all(const Radius.circular(45.0)),
      ), 
      height: mHeight,
    //  padding: EdgeInsets.only(right: 500  * (1.0 - _animation.value) ),
    //  padding: EdgeInsets.only(bottom: (4*mHeight) * (1.0 - _animation.value) ),
      margin: const EdgeInsets.only(left: 10, right: 10, bottom:5),
      child: InkWell(
        splashColor: Color.fromARGB(255, 255, 255, 255),
        borderRadius: BorderRadius.all(const Radius.circular(45.0)),
        onLongPress: onLongPress,
        onTap: (){
          GlobalVariable.inst().getEvent("OnAccountPageTab").send(null);
        },
        radius: 500,
        child: Row(
        //  mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[ 
            WCategory(mCategory:_category, mHeight: mHeight,),
            Expanded(
              flex: 70,
              child: Container(
                decoration: BoxDecoration(
                  color: Color.fromARGB(100, 20, 20, 20),
                  borderRadius: BorderRadius.all(const  Radius.circular(5.0)),
                ),
              //  color: Colors.white10,
                child: Align(
                  alignment: Alignment(-0.9, 0), 
                  child: Text(_memo,
                    style: TextStyle(
                      fontSize: 10, 
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
            WSpacer(width:3),
            WCost(cost: _cost, width: 80, height: mHeight,),
          ],
        )
      ),
    );
  }

  Widget editor(){
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(mHeight)),
        color: Colors.white,
      ),
      //margin: const EdgeInsets.only(bottom:5),
      margin: const EdgeInsets.only(left: 10, right: 10, bottom:5),
      height: mHeight,
      child: Row(
        children: [
          Container(
            height: mHeight,
            decoration: BoxDecoration(
            color: Colors.black26,
       //     borderRadius: BorderRadius.only(topLeft: Radius.circular(30), bottomLeft: Radius.circular(30), topRight: Radius.circular(15), bottomRight: Radius.circular(15)),
          ),
            child: DropdownButtonHideUnderline(
              child: ButtonTheme(
                alignedDropdown: false,
              // color: Colors.white,
                child:  DropdownButton(
                  iconDisabledColor: Colors.black38,
                  isDense: false,
                  isExpanded: false,
                  value: _category,
                  iconSize: mHeight,
                  iconEnabledColor: Colors.white,
                  items: UserData.inst().createCategoryMenuItem(),
                  onChanged: onCategoryChange,
                ),
              ),
            ),
          ),
          WSpacer(width: 5),
          Expanded(
            flex: 10,
            child: Container(
              height: mHeight,
              decoration: BoxDecoration(
                color: Colors.black38,
                borderRadius: BorderRadius.all(const Radius.circular(5.0)),
              ),
              child: TextField(
                controller: _cntrMemoField,
                textInputAction: TextInputAction.done,
              //  maxLength: 30,
              //  maxLines: 1,
              //  textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 10,
                  color: Colors.white,
                ),
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.only(left: 2, top: 2, bottom: 0),
                  focusedBorder: InputBorder.none,
                  border: InputBorder.none,
                  hintText: "메모",
                  counterText: "",
                ),
              ),
            ),
          ),
          WSpacer(width: 5,),
          Expanded(
            flex: 5,
            child: Container(
              height: mHeight,
              decoration: BoxDecoration(
                color: Colors.black38,
                borderRadius: BorderRadius.all(const Radius.circular(5.0)),
              ),
              child: TextField(
                controller: _cntrCostField,
                textInputAction: TextInputAction.done,
                keyboardType: TextInputType.number,
                textAlign: TextAlign.right,
                style: TextStyle(
                  fontSize: 10,
                  color: Colors.white,
                ),
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.only(right: 0, top: 2, bottom: 0),
                  focusedBorder: InputBorder.none,
                  border: InputBorder.none,
                  hintText: "0_",
                  counterText: "",
                ),
                inputFormatters: [
                  CurrencyFormat(),
                ],
              ),
            ),
          ),
          WSpacer(width: 5,),
          Container(
            width: mHeight,
            height: 30,
            decoration: BoxDecoration(
              color: Color.fromARGB(100, 20, 20, 20),
              borderRadius: BorderRadius.all(const Radius.circular(45.0)),
            ),
          //  color: Colors.white10,
            child: Align(
              alignment: Alignment.center,
              child: Text("(₩)",
                style: TextStyle(
                  fontSize: 10, 
                  fontWeight: FontWeight.bold,
                  color: Colors.green,
                ),
              ),
            )
          ),
        ],
      ),
    );
  }

  void onCategoryChange (String value){
    setState(() {
      _prevCategory = _category;
      _category = value;
    });
  }

  Future onAccountPageTab(dynamic data) async{

    int newCost = _cntrCostField.text != "" ? int.parse(_cntrCostField.text.replaceAll(new RegExp(','), '')) : 0;
    if(_category != _prevCategory ||_memo != _cntrMemoField.text || _cost != newCost){
      _memo = _cntrMemoField.text;
      _cost = newCost;

      await UserData.inst().queryAccount("""
        UPDATE AccountData SET 
          category = '$_category', 
          memo = '$_memo', 
          cost = $_cost 
        WHERE 
          key = $_key
      """);

      GlobalVariable.inst().getStream("account_page").add("update");
      GlobalVariable.inst().getStream("account_page").add("redraw");
    }
    
    _isEditMode = false;
    GlobalVariable.inst().getEvent("OnAccountPageTab").unregister(onAccountPageTab);
    _overlayEntry.remove();
    setState(() {});
  }
  
  Future onLongPress() async{
    //await Future.delayed(const Duration(milliseconds: 200), () => "1");
    _isEditMode = true;
    GlobalVariable.inst().getEvent("OnAccountPageTab").register(onAccountPageTab);

    _overlayEntry = showToolOverlay(context, this);
    setState(() {});
  }

  OverlayEntry showToolOverlay(BuildContext context, WAccountItemState item) {
    double iconSize = 30;
    final screenSize = MediaQuery.of(context).size;
   
    var pos = getPosition(context);
    var size = getSize(context);

    Offset newPos = Offset(screenSize.width/2 - iconSize/2, pos.dy + size.height/2 - iconSize/2 + 50);

    // print(MediaQuery.of(context).size);
    // print(pos);
    // print(size);

    OverlayState overlayState = Overlay.of(context);
    OverlayEntry overlayEntry = OverlayEntry(
      builder: (context) => Positioned(
        left: newPos.dx,
        top: newPos.dy,
        child: Container(
          // decoration: BoxDecoration(
          //   color: Colors.transparent,
          //   borderRadius: BorderRadius.all(const  Radius.circular(30.0)),
          //   border: Border.all(
          //     color: Colors.white24,
          //   ),
          // ),
          //margin: const EdgeInsets.only(left: 135, right: 135, top:5, bottom:5),
          child: Material(
            color: Colors.transparent,
            child: CustumButton(
              iconSize: iconSize,
              shape: CircleBorder(),
              foregroundColor: Color.fromARGB(255, 255, 255, 255),
              backgroundColor: Color.fromARGB(255, 255, 40, 40),
              iconData: FontAwesomeIcons.trash,
              onPressed: () async{
                GlobalVariable.inst().getEvent("OnAccountPageTab").send(null);
                
                var result = await showDialog(
                  context: context,
                  builder: (content) => WDialog("삭제 하시겠습니까?"),
                );

                if(result != null && result["confirm"] == true){
                  await UserData.inst().deleteItem(data: {"key": item._key});
                  GlobalVariable.inst().getStream("account_page").add("update");
                  GlobalVariable.inst().getStream("account_page").add("redraw");
                }
              },
            )
          ),
        ),
      ));

    overlayState.insert(overlayEntry);
    return overlayEntry;
  }
}

Size getSize(BuildContext context){
   RenderBox renderBox = context.findRenderObject();
   //print(renderBox.size);
    return renderBox.size;
}

Offset getPosition(BuildContext context){
   RenderBox renderBox = context.findRenderObject();
  // print(renderBox.localToGlobal(Offset.zero));
    return renderBox.localToGlobal(Offset.zero);
}
