import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_test_00/custum_wiget_tool.dart';
import 'date_divider.dart';
import 'package:flutter_test_00/user_data.dart';

import 'package:flutter_test_00/account_widget/cost.dart';
import 'item.dart';
import 'item_edit_bar.dart';
import 'search_helper.dart';

class WAccountList extends StatefulWidget{
  final int year;
  final int month;
  final StreamController streamTextField;

  WAccountList({this.year=0, this.month=0, this.streamTextField});

  @override State<StatefulWidget> createState() {
    return WAccountListState();
  }
}

class WAccountListState extends State<WAccountList>{
  String category = "전체";
  String memo = "";

  @override
  void initState() {
    widget.streamTextField.stream.listen(
      (data) {
        if(data["memo"] != null)
          memo = data["memo"];
        if(data["category"] != null)
         category = data["category"];
        setState(() {});
      }, 
    );
    super.initState();
  }
  @override Widget build(BuildContext context) {

    String query = "";
    query += "SELECT * FROM AccountData ";
    if(widget.year != 0){
      query += "WHERE ";
      query +=   "year == ${widget.year} ";

      if(widget.month != 0){
        query +=     "AND "; 
        query +=   "month == ${widget.month} ";
      }
    }

    if(category != "전체"){
      query +=     "AND "; 
      query +=   "category == '${category}' ";
    }
    if(memo != ""){
      query +=     "AND "; 
      query +=   "memo LIKE '%${memo}%' ";
    }

    query += "ORDER BY ";
    if(widget.year == 0)
      query +=   "year, ";
    if(widget.month == 0)
      query +=   "month, ";
    query +=   "day ";
    //query +=   ", ";
    if(widget.month != 0){
      query +=   "DESC, ";
      query +=   "idx ";
    }


    return FutureBuilder(
      future: UserData.inst().queryAccount(query), // a previously-obtained Future<String> or null
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
            return ListView(
              children: <Widget>[],
            );
          case ConnectionState.active:
          case ConnectionState.waiting:            
            return SpinKitFoldingCube (
              color: Colors.white,
              size: 100.0,
            );
          case ConnectionState.done:
            if (snapshot.hasError)
              return Text('Error: ${snapshot.error}');
            var lstAccount = snapshot.data;

            DateTime pvtDate;
            var lstAccountWidget = List<Widget>();
            double dailyCost = 0;

            for (var item in lstAccount) {
              DateTime time = DateTime(item.year, item.month, item.day);
              if(pvtDate != time){
                if(dailyCost != 0){
                  lstAccountWidget.add(WDailyCost(cost: dailyCost, width: 80));
                  dailyCost = 0;
                }
                pvtDate = time;
                lstAccountWidget.add(WDateDivider(date: pvtDate));
              }
              lstAccountWidget.add(WAccountItem(data: item));
              dailyCost += item.cost;
            }

            if(dailyCost != 0)
              lstAccountWidget.add(WDailyCost(cost: dailyCost, width: 80));

            return ListView(
              padding: EdgeInsets.only(top:0, bottom: 40),
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              children: lstAccountWidget,
            );
        }
        return ListView(
          children: <Widget>[],
        );
      },
    );


    return FutureBuilder(
      future: UserData.inst().queryAccount(query), // a previously-obtained Future<String> or null
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
            return SliverList(
          delegate: SliverChildListDelegate([]),
        );
          case ConnectionState.active:
          case ConnectionState.waiting:            
            return SliverFillRemaining(
                child: SpinKitFoldingCube (
                  color: Colors.white,
                  size: 100.0,
                ),
            );
          case ConnectionState.done:
            if (snapshot.hasError)
                return Text('Error: ${snapshot.error}');
            var lstAccount = snapshot.data;

            var lstAccountWidget = List<Widget>();
            DateTime pvtDate;

            // lstAccountWidget.add(
            //   Container(
            //     color: Colors.white,
            //     height: 0.5,
            //     margin: const EdgeInsets.only(left:20.0, right:20.0, bottom:10.0),
            //   ),
            // );
            //lstAccountWidget.add(WSpacer(height: 5,));
            //lstAccountWidget.add(WAccountEdit());

            for (var item in lstAccount) {
              DateTime time = DateTime(widget.year, widget.month, item.day);
              if(pvtDate != time){
                pvtDate = time;
                lstAccountWidget.add(WDateDivider(date: pvtDate));
              }
              lstAccountWidget.add(WAccountItem(data: item));
            }

            return SliverFixedExtentList(
              itemExtent: 30,
              delegate: SliverChildBuilderDelegate(
                (context, index){
                  return lstAccountWidget[index];
                },
                childCount: lstAccountWidget.length,
              ),
            );
        }
        return SliverList(
          delegate: SliverChildListDelegate([]),
        );
      },
    );
  }
}



  // String query = """SELECT * FROM AccountData 
  //         WHERE 
  //           year == $year AND 
  //           month == $month AND
  //           category == '$category'
  //         ORDER BY  
  //           day,
  //           idx
  //       """;