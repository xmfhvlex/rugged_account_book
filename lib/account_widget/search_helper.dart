import 'dart:ui';
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'category.dart';
import 'package:flutter_test_00/custum_wiget_tool.dart';
import 'package:flutter_test_00/user_data.dart';

//Intl.defaultLocale = 'pt_BR';

class WSearchHelper extends StatefulWidget {
  final StreamController streamController;

  WSearchHelper({this.streamController});

  @override WSearchHelperState createState(){
    return WSearchHelperState();
  }
}

class WSearchHelperState extends State<WSearchHelper> {
  String _currentItem = "전체";

  @override Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.black38,
      ),
      height: 30,
      child: Row(
        children: [
          Container(
          //  color: Colors.black38,
            width: 115,
            child: DropdownButtonHideUnderline(
              child: ButtonTheme(
                alignedDropdown: false,
              // color: Colors.white,
                child:  DropdownButton(
                  iconDisabledColor: Colors.black38,
                  isDense: true,
                  isExpanded: true,
                  value: _currentItem,
                  hint: Text("Hint"),
                  iconSize: 30,
                  iconEnabledColor: Colors.white,
                  items: UserData.inst().createCategoryMenuItem(isItem: false),
                  onChanged: (String value){
                    _currentItem = value;
                    widget.streamController.add({"category":value});
                    setState(() {});
                  }
                ),
              ),
            ),
          ),
          WSpacer(width: 5),
          Expanded(
            flex: 10,
            child: Container(
              color: Colors.black38,
              child: TextField(
                textInputAction: TextInputAction.done,
                keyboardAppearance: Brightness.dark,
                maxLength:10,
                maxLines: 1,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 12,
                  color: Colors.white,
                ),
                decoration: InputDecoration(
                  hintText: "검색 내용",
                  hintStyle: TextStyle(height:0),
                  counterText: "",
                  counterStyle: TextStyle(
                    color: Colors.white,
                  ),
                ),
                onChanged: (value){
                  widget.streamController.add({"memo":value});
                },
                onTap: (){

                },
                // onFieldSubmitted: (value){
                //     _inputFieldValue = value;
                //   setState(() {});
                // },
                //onSaved: (value){
//
                //  print(value);
                //},
                onEditingComplete: null,
                inputFormatters: [
                //  LengthLimitingTextInputFormatter(1),
                ],
              ),
            ),
          ),
          WSpacer(width: 35),
        ],
      ),
    );
  }
}