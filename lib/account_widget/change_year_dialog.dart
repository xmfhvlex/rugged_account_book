import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_test_00/custum_wiget_tool.dart';
import 'package:numberpicker/numberpicker.dart';

import 'package:flutter_test_00/user_data.dart';


class WChangeYearDialog extends StatefulWidget {
  int _initYear;
  WChangeYearDialog({Key key, int initYear}) : super(key: key) {
    _initYear = initYear;
  }

  @override WChangeYearDialogState createState(){
    return WChangeYearDialogState(_initYear);
  }
}

class WChangeYearDialogState extends State<WChangeYearDialog> {
  int _pvtYear;
  
  WChangeYearDialogState(int initYear){
    _pvtYear = initYear;
  }

  @override void initState() {
    super.initState();
  }

  Future selectDate(BuildContext context) async{
    setState(() {});
  }

  @override Widget build(BuildContext context) {
    return SimpleDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(const Radius.circular(30.0))
      ),
      backgroundColor: Color.fromARGB(255, 0, 0, 0),
      elevation: 10,
      title: Align(
        child: Text("Select Year")
      ),
      children: <Widget>[
        WSpacer(height: 50,),
        NumberPicker.integer(
          listViewWidth: 50,
          initialValue: _pvtYear,
          minValue: 2000,
          maxValue: 2030,
          onChanged: (newValue){
            _pvtYear = newValue;
            setState((){});
          },
        ),
        WSpacer(height: 50,),
        Container(
          child: Row(
            children: [
              WSpacer(width: 30),
              Container(
                decoration: BoxDecoration(
                  color: Colors.red,
                  borderRadius: BorderRadius.all(const Radius.circular(10.0)),
                ),
                child: SimpleDialogOption(
                  child: Text('취   소'),
                  onPressed: (){
                    Navigator.pop(context, null);
          //          SystemChrome.setEnabledSystemUIOverlays([]);
                  },
                ),
              ),
              Expanded(
                child: Container(
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  color: Colors.blue,
                  borderRadius: BorderRadius.all(const Radius.circular(10.0)),
                ),
                child: SimpleDialogOption(
                  child: Text('선   택'),
                  onPressed: (){
                    List lstYear = UserData.inst().dataCache["list_year"];
                    Navigator.pop(context, lstYear.contains(_pvtYear)==true ? <String, dynamic>{"year": _pvtYear} : null);
              //      SystemChrome.setEnabledSystemUIOverlays([]);
                  },
                ),
              ),
              WSpacer(width: 30,),
            ]
          ),
        ),
      ],

      // title: Container(
      //   child: Align(
      //     child: Text("Select Year",
      //       style: TextStyle(
      //         color: Colors.white,
      //       ),
      //     ),
      //   ),
      // ),
      // children: <Widget>[
      //   new Padding(
      //     padding: const EdgeInsets.symmetric(vertical: 100.0, horizontal: 0.0),
      //     child: new NumberPicker.integer(
      //       initialValue: _year,
      //       minValue: 0,
      //       maxValue: 100,
      //       onChanged: (newValue) =>
      //         setState(() => _year = newValue)),
      //   )
      // ],
    );
  }
}
