import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_test_00/custum_wiget_tool.dart';

import 'package:flutter_test_00/user_data.dart';
import 'package:auto_size_text/auto_size_text.dart';

class WCategory extends StatelessWidget{
  final String mCategory;
  Icon _icon;
  bool mIsChart;
  double mHeight;
  double mIconScale;

  WCategory({this.mCategory, this.mIsChart = false, this.mHeight = 25, this.mIconScale = 1});

  @override Widget build(BuildContext context) {
    // var t = AutoSizeText(
    //   '그게뭔말인데요',
    //   style: TextStyle(fontSize: 10),
    //   maxLines: 1,
    //   minFontSize: 6,
    // ); 

    return Row(
      children: <Widget>[
        Container(
          width: mHeight,
          height: mHeight,
          decoration: BoxDecoration(
            color: Colors.black26,
            borderRadius: BorderRadius.circular(mHeight),
          ),
          child: Icon(
            Icons.fastfood, 
          //  FontAwesomeIcons.gamepad,
            size: mHeight * 0.6 * mIconScale,
            color: UserData.inst().getCategoryColor(mCategory),
          ),
        ),
        WSpacer(width:5, color:Colors.transparent),
        Container(
          decoration: BoxDecoration(
            color: UserData.inst().getCategoryColor(mCategory),
            borderRadius: (mIsChart) ? BorderRadius.only(topLeft: const Radius.circular(5.0), bottomLeft: const Radius.circular(5.0)) : 
                                      BorderRadius.all(const Radius.circular(5.0))
          ),
          margin: const EdgeInsets.only(right: 5.0),
          width: 45,
          height: mHeight,
          child: Align(
            alignment: Alignment.center,
            // child: AutoSizeText(
            //   '그게뭔말인데요',
            //   style: TextStyle(fontSize: 10),
            //   maxLines: 1,
            //   minFontSize: 6,
            // ) 
            child: BorderedText(
              strokeWidth: 1,
              child: Text(mCategory,
                style: TextStyle(
                  fontSize: 10, 
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
