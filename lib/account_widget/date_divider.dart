import 'dart:ui';
import 'add_account_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test_00/custum_wiget_tool.dart';
import 'package:flutter_test_00/global/globalVariable.dart';
import 'package:flutter_test_00/user_data.dart';


class WDateDivider extends StatelessWidget {
  final DateTime date;

  WDateDivider({this.date});

  @override Widget build(BuildContext context) {
    Color txtColor;

    switch(date.weekday){
      case 6:
        txtColor = Colors.cyan[200];
        break;
      case 7:
        txtColor = Colors.pink[200];
        break;
      default:
        txtColor = Colors.white;
        break;
    }

    return Container(
      decoration: BoxDecoration(
        color: Color.fromARGB(50, 20, 20, 20),
        borderRadius: BorderRadius.all(const  Radius.circular(45.0)),
        border: Border.all(
          color: Colors.white24,
        ),
      ),
      margin: const EdgeInsets.only(left: 135, right: 135, top:5, bottom:5),
      height: 15,
      child: InkWell(
        splashColor: Color.fromARGB(255, 255, 255, 255),
        radius: 200,
        borderRadius: BorderRadius.all(const Radius.circular(45.0)),
        onTap: () async{
          GlobalVariable.inst().getEvent("OnAccountPageTab").send(null);
          await Future.delayed(const Duration(milliseconds: 200));
          var item = await showDialog(
            context: context,
            builder: (content) => WAccountAddDialog(date: date, isSelectedDate: true,),
          );
          if(item != null){
            await UserData.inst().addItem(data: item);
            GlobalVariable.inst().getStream("account_page").add("update");
            GlobalVariable.inst().getStream("account_page").add("redraw");
          }
        },
        child: Center(
          child: Text(gdate1Formatter.format(date),
              style: TextStyle(
              fontSize: 10, 
              color: txtColor,
            ),
          ),
        )
      ),
    );
  }
}