import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter_test_00/custum_wiget_tool.dart';

class WCost extends StatelessWidget{
  num     _cost = 0;
  double  _width = 100;
  double _height = 30;

  WCost({num cost, double width, double height}){
    _cost = cost;
    _width = width;
    _height = height;
  }

  @override Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
       // color: Color.fromARGB(50, 0, 0, 0),
       // borderRadius: BorderRadius.all(const Radius.circular(45.0)),
      ), 
      height: _height,
      //margin: const EdgeInsets.all(2.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[ 
          Container(
            width: _width,
            decoration: BoxDecoration(
              color: Color.fromARGB(100, 20, 20, 20),
              borderRadius: BorderRadius.all(const Radius.circular(5.0)),
            ),
            child: Align(
              alignment: Alignment(0.9, 0),
              child: BorderedText(
                strokeWidth: 1,
                strokeColor: Colors.black,
                child: Text(gCostFormatter.format(_cost),
                  style: TextStyle(
                    fontSize: 10, 
                    color: Colors.white,
                  ),
                )
              ),
            ),
          ),
          WSpacer(width: 3),
          Container(
            width: _height,
            decoration: BoxDecoration(
              color: Color.fromARGB(100, 20, 20, 20),
              borderRadius: BorderRadius.all(const Radius.circular(45.0)),
            ),
          //  color: Colors.white10,
            child: Align(
              alignment: Alignment(0.08, 0),
              child: Text("₩",
                style: TextStyle(
                  fontSize: 15, 
                  fontWeight: FontWeight.w300,
                  color: Colors.white,
                ),
              ),
            )
          ),
        ],
      ),
    );
  }
}

class WDailyCost extends StatelessWidget{
  num     _cost = 0;
  double  _width = 100;

  WDailyCost({num cost, double width}){
    _cost = cost;
    _width = width;
  }

  @override Widget build(BuildContext context) {
    return Container(
      height: 15,
      margin: const EdgeInsets.only(right: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[ 
          Container(
            width: _width,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(const Radius.circular(5.0)),
            ),
            child: Align(
              alignment: Alignment(0.9, 0),
              child: BorderedText(
                strokeWidth: 1,
                strokeColor: Colors.black,
                child: Text(gCostFormatter.format(_cost),
                  style: TextStyle(
                    fontSize: 10, 
                    color: Colors.green,
                  ),
                )
              ),
            ),
          ),
          WSpacer(width: 3),
          Container(
            width: 25,
            decoration: BoxDecoration(
         //     color: Color.fromARGB(100, 20, 20, 20),
              borderRadius: BorderRadius.all(const Radius.circular(45.0)),
            ),
          //  color: Colors.white10,
            child: Align(
              alignment: Alignment.center,
              child: Text("₩",
                style: TextStyle(
                  fontSize: 15, 
                  fontWeight: FontWeight.w300,
                  color: Colors.green,
                ),
              ),
            )
          ),
        ],
      ),
    );
  }
}
