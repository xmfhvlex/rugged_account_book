import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'category.dart';
import 'package:flutter_test_00/custum_wiget_tool.dart';
import 'package:flutter_test_00/user_data.dart';

//Intl.defaultLocale = 'pt_BR';

class WAccountEdit extends StatefulWidget {
  WAccountEdit({Key key}) : super(key: key);

  @override WAccountEditState createState(){
    return WAccountEditState();
  }
}

class WAccountEditState extends State<WAccountEdit> {
  String _currentItem = "식사";
  String _inputFieldValue = "메모";

  void onChanged (String value){
    setState(() {
      _currentItem = value;
    });
  }

  @override Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(const Radius.circular(100.0)),
        color: Colors.black12,
      ),
      height: 30,
      child: Row(
        children: [
          // Expanded(
          //   flex: 5,
          //   child: Container(
          //     color: Colors.black38,
          //     child: TextFormField(
          //       initialValue: "0",
          //       textAlign: TextAlign.right,
          //       keyboardType: TextInputType.datetime,
          //       style: TextStyle(
          //         fontSize: 10,
          //         color: Colors.white,
          //       ),
          //       decoration: InputDecoration(
          //       //  labelStyle: TextStyle(
          //       //    color: Colors.white,
          //       //  ),
          //       ),
          //       onEditingComplete: null,
          //       onFieldSubmitted: (value){
          //         SystemChrome.setEnabledSystemUIOverlays([]);
          //       },
          //       inputFormatters: [
          //         CurrencyFormat(),
          //       ],
          //     ),
          //   ),
          // ),
          // WSpacer(width: 5,),
          Container(
            color: Colors.black38,
            width: 115,
            child: DropdownButtonHideUnderline(
              child: ButtonTheme(
                alignedDropdown: false,
              // color: Colors.white,
                child:  DropdownButton(
                  iconDisabledColor: Colors.black38,
                  isDense: true,
                  isExpanded: true,
                  value: _currentItem,
                  hint: Text("Hint"),
                  iconSize: 30,
                  iconEnabledColor: Colors.white,
                  items: UserData.inst().createCategoryMenuItem(),
                  onChanged: onChanged,
                ),
              ),
            ),
          ),
          WSpacer(width: 5),
          Expanded(
            flex: 10,
            child: Container(
              color: Colors.black38,
              child: TextFormField(
                initialValue: _inputFieldValue,
                textInputAction: TextInputAction.done,
                keyboardAppearance: Brightness.dark,
                maxLength:10,
                maxLines: 1,
              //  autofocus: true,
              //  textDirection: prefix0.TextDirection.rtl,
              //  autofocus: true,
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontSize: 12,
                  color: Colors.white,
                ),
                decoration: InputDecoration(
                  counterText: "",
                  counterStyle: TextStyle(
                    color: Colors.white,
                  ),
                //  labelText: "- 메모 -",
                //  labelStyle: TextStyle(
                //    color: Colors.white,
                //  ),
                ),
                onFieldSubmitted: (value){
                  setState(() {
                    _inputFieldValue = value;
                    if(_inputFieldValue.isEmpty == true){
                      _inputFieldValue = "메모";
                    }
           //         SystemChrome.setEnabledSystemUIOverlays([]);

                  });
                },
                onEditingComplete: null,
                inputFormatters: [
                //  LengthLimitingTextInputFormatter(1),
                ],
              ),
            ),
          ),
          WSpacer(width: 5,),
          Expanded(
            flex: 5,
            child: Container(
              color: Colors.black38,
              child: TextFormField(
                initialValue: "0",
                textAlign: TextAlign.right,
                keyboardType: TextInputType.number,
                style: TextStyle(
                  fontSize: 10,
                  color: Colors.white,
                ),
                decoration: InputDecoration(
                //  labelStyle: TextStyle(
                //    color: Colors.white,
                //  ),
                ),
                onEditingComplete: null,
                onFieldSubmitted: (value){
         //         SystemChrome.setEnabledSystemUIOverlays([]);
                },
                inputFormatters: [
                  CurrencyFormat(),
                ],
              ),
            ),
          ),
          WSpacer(width: 5,),
          Container(
            width: 30,
            height: 30,
            decoration: BoxDecoration(
              color: Color.fromARGB(100, 20, 20, 20),
              borderRadius: BorderRadius.all(const Radius.circular(45.0)),
            ),
          //  color: Colors.white10,
            child: Align(
              alignment: Alignment.center,
              child: Text("(₩)",
                style: TextStyle(
                  fontSize: 10, 
                  fontWeight: FontWeight.bold,
                  color: Colors.green,
                ),
              ),
            )
          ),
        ],
      ),
    );
  }
}