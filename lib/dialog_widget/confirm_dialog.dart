import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_test_00/custum_wiget_tool.dart';

class WDialog extends StatefulWidget {
  final String title;
  WDialog(this.title, {Key key}) : super(key: key);

  
  @override WDialogState createState(){
    return WDialogState();
  }
}

class WDialogState extends State<WDialog> {
  @override void initState() {
    super.initState();
  }

  @override Widget build(BuildContext context) {
    return SimpleDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(const Radius.circular(30.0))
      ),
      backgroundColor: Color.fromARGB(255, 50, 50, 50),
      elevation: 10,
      title: Container(
        child: Align(
          child: Text(widget.title,
            style: TextStyle(
              color: Colors.white,
            ),
          ),
        ),
      ),
      children: <Widget>[
        Container(
          child: Row(
            children: [
              WSpacer(width: 50),
              Container(
                decoration: BoxDecoration(
                  color: Colors.red,
                  borderRadius: BorderRadius.all(const Radius.circular(10.0)),
                ),
                child: SimpleDialogOption(
                  child: Text('취   소'),
                  onPressed: (){
                    Navigator.pop(context, <String, dynamic>{ "confirm": false});
            //         SystemChrome.setEnabledSystemUIOverlays([]);
                  },
                ),
              ),
              Expanded(
                child: Container(
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  color: Colors.blue,
                  borderRadius: BorderRadius.all(const Radius.circular(10.0)),
                ),
                child: SimpleDialogOption(
                  child: Text('확   인'),
                  onPressed: (){
                    Navigator.pop(context, <String, dynamic>{ "confirm": true});
                  },
                ),
              ),
              WSpacer(width: 50,),
            ]
          ),
        ),
      ],
    );
  }
}