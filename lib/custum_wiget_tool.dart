import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter/services.dart';
import 'dart:ui';
import 'package:flutter_test_00/global/globalVariable.dart';
import 'account_widget/add_account_dialog.dart';

final gDayFormatter = NumberFormat("##00");
final gCostFormatter = NumberFormat("#,###");
final gPercentFormatter = NumberFormat("##.##");
final gdate1Formatter = DateFormat('yyyy-MM-dd (EEE)');
final gdate2Formatter = DateFormat('yyyy-MM-dd');

var gNumberComma = RegExp('[\\,]');

//Intl.defaultLocale = 'pt_BR';

final f = NumberFormat.currency(
  decimalDigits: 0,
  symbol: " (₩)",
);

enum Answers{YES ,NO, MAYBE}

var glstColor = [
  Color.fromARGB(255, 230, 25, 75), 
  Color.fromARGB(255, 60, 180, 75), 
  Color.fromARGB(255, 255, 225, 25), 
  Color.fromARGB(255, 0, 130, 200), 
  Color.fromARGB(255, 245, 130, 48), 
  Color.fromARGB(255, 145, 30, 180), 
  Color.fromARGB(255, 70, 240, 240), 
  Color.fromARGB(255, 240, 50, 230), 
  Color.fromARGB(255, 210, 245, 60), 
  Color.fromARGB(255, 250, 190, 190), 
  Color.fromARGB(255, 0, 128, 128), 
  Color.fromARGB(255, 230, 190, 255), 
  Color.fromARGB(255, 170, 110, 40), 
  Color.fromARGB(255, 255, 250, 200), 
  Color.fromARGB(255, 128, 0, 0), 
  Color.fromARGB(255, 170, 255, 195), 
  Color.fromARGB(255, 128, 128, 0), 
  Color.fromARGB(255, 255, 215, 180), 
  Color.fromARGB(255, 0, 0, 128), 
  Color.fromARGB(255, 128, 128, 128), 
  Color.fromARGB(255, 255, 255, 255), 
  Color.fromARGB(255, 0, 0, 0)
];

RegExp regexPrice = RegExp(r"[^\d]");
class CurrencyFormat extends TextInputFormatter {
  @override TextEditingValue formatEditUpdate( TextEditingValue oldValue, TextEditingValue newValue) {
    if (newValue.text.length > 20)
      return oldValue;
    
    String strNewValue = newValue.text;
    //num data = num.parse(newValue.text.replaceAll(RegExp(r"[^\d]"), ''));
    if(strNewValue.contains("-")){
      if(strNewValue[0] == "-"){
        strNewValue = strNewValue.replaceAll("-", "");
      }
      else{
        strNewValue = strNewValue.replaceAll("-", "");
        strNewValue = "-" + strNewValue;
      }
    }

    if(strNewValue != ""){
      int number = int.parse(strNewValue.replaceAll(',', ''));
      strNewValue = gCostFormatter.format(number);
    }

    return TextEditingValue(
      selection: TextSelection.fromPosition(
        TextPosition(offset: strNewValue.length)
      ),
      text: strNewValue
    );
  }
}

class WSpacer extends StatelessWidget {
  final double  width;
  final double  height;
  final Color   color;

  WSpacer({this.width=0, this.height=0, this.color=Colors.transparent});

  @override Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      color: color,
    );
  }
}

class CustumButton extends StatelessWidget {
  final ShapeBorder shape;
  final Color foregroundColor;
  final Color backgroundColor;
  final IconData iconData;
  final double iconSize;
  final VoidCallback onPressed;
  final double width;

  CustumButton({ Key key, this.width=50, this.iconSize=24, this.shape, this.foregroundColor, this.backgroundColor, this.iconData, this.onPressed }) : super(key: key);

  @override Widget build(BuildContext context){
    return Container(
      width: width,
      child: Ink(
        // decoration: BoxDecoration(
        //   border: Border.all(
        //     color: Colors.white54,
        //   ),
        //   color: Color.fromARGB(100, 20, 20, 20),
        //   borderRadius: BorderRadius.all(const Radius.circular(45.0)),
        // ),
        decoration: ShapeDecoration(
          color: backgroundColor,
          shape: shape,
           // decoration: BoxDecoration(
          //   color: Colors.transparent,
          //   borderRadius: BorderRadius.all(const  Radius.circular(30.0)),
          //   border: Border.all(
          //     color: Colors.white24,
          //   ),
          // ),
        ),

        child: IconButton(
          icon: Icon(iconData),
          iconSize: iconSize,
          color: foregroundColor,
          onPressed: onPressed,
        ),
      ),
    );
  }
}

class BorderedText extends StatelessWidget {
  BorderedText({
    this.strokeCap = StrokeCap.round,
    this.strokeJoin = StrokeJoin.round,
    this.strokeWidth = 6.0,
    this.strokeColor = const Color.fromRGBO(53, 0, 71, 1),
    @required this.child,
  }) : assert(child != null);
  /// the stroke cap style
  final StrokeCap strokeCap;
  /// the stroke joint style
  final StrokeJoin strokeJoin;
  /// the stroke width
  final double strokeWidth;
  /// the stroke color
  final Color strokeColor;
  /// the `Text` widget to apply stroke on
  final Text child;
  @override
  Widget build(BuildContext context) {
    TextStyle style;
    if (child.style != null) {
      style = child.style.copyWith(
        foreground: Paint()
          ..style = PaintingStyle.stroke
          ..strokeCap = strokeCap
          ..strokeJoin = strokeJoin
          ..strokeWidth = strokeWidth
          ..color = strokeColor,
        color: null,
      );
    } else {
      style = TextStyle(
        foreground: Paint()
          ..style = PaintingStyle.stroke
          ..strokeCap = strokeCap
          ..strokeJoin = strokeJoin
          ..strokeWidth = strokeWidth
          ..color = strokeColor,
      );
    }

    return Stack(
      alignment: Alignment.center,
    //  textDirection: Text.TextDirection.ltr,
      
      children: [
        Text(
          child.data,
          style: style,
          maxLines: child.maxLines,
          overflow: child.overflow,
          semanticsLabel: child.semanticsLabel,
          softWrap: child.softWrap,
          strutStyle: child.strutStyle,
          textAlign: child.textAlign,
          textDirection: child.textDirection,
          textScaleFactor: child.textScaleFactor,
        ),
        child,
      ],
    );
  }
}

Widget blur(){
 // if(
 //     //dialog pops up or is active
 // ){
    return BackdropFilter( filter: ImageFilter.blur(sigmaX:5.0,sigmaY:5.0), );
//  }
//  else
//    return Image.asset('asset url', fit: BoxFit.cover);////if dialog not active returns an unfiltered image
}