import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart' show DragStartBehavior;
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_test_00/account_widget/item.dart';
import 'package:flutter_test_00/account_widget/search_helper.dart';
import 'package:random_string/random_string.dart';

import 'account_widget/change_year_dialog.dart';
import 'account_widget/add_account_dialog.dart';
import 'account_widget/chart.dart';
import 'custum_wiget_tool.dart';
import 'account_widget/item_list.dart';
import 'user_data.dart';
import 'global/globalVariable.dart';

int n = 0;

class MonthView extends StatefulWidget {
  final int year;
  final int month;

  MonthView( {Key key, this.year, this.month} ) : super(key: key);
  @override State createState() => MonthViewState();
}

class MonthViewState extends State<MonthView> {
  StreamController  _streamTextField;
  ScrollController _scrollController;

  @override void initState() {
    print("[MonthView ${widget.month}] initState" + "---${widget.hashCode}-----------------------------------------------${n++}");
    super.initState();
    _streamTextField = new StreamController();
    _scrollController = new ScrollController();
    GlobalVariable.inst().pageDateTime = DateTime(widget.year, widget.month);
    print(GlobalVariable.inst().pageDateTime.toString() + "+_+_+_+_+_+_+_+_+");

  }

  @override void deactivate() { 
    print("[MonthView ${widget.month}] deactivate" + "---${widget.hashCode}----${n++}");
    super.deactivate();
  }

  @override void didUpdateWidget(MonthView oldWidget) {
    print("[MonthView ${widget.month}] didUpdateWidget" + "---${widget.hashCode}----${n++}");
    super.didUpdateWidget(oldWidget);
  }

  @override void didChangeDependencies() { 
    print("[MonthView ${widget.month}] didChangeDependencies" + "---${widget.hashCode}----${n++}");
    super.didChangeDependencies();
  }

  @override void dispose() {
    print("[MonthView ${widget.month}] dispose" + "---${widget.hashCode}-----------------------------------${n++}");
    _streamTextField.close();
    _scrollController.dispose();
    super.dispose();
  }

  @override Widget build(BuildContext context){
    print("[MonthView ${widget.month}] build" + "---${widget.hashCode}-----------------------------------${n++}");

    double minFlexibleHeight = MediaQuery.of(context).size.height/3;
    double expandHeight = 30.0 * (UserData.inst().categoryData.length-1) + 30 + 5;

    return new NestedScrollView(
      scrollDirection: Axis.vertical,
      headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {  
        return <Widget>[
          SliverAppBar(
            titleSpacing: 0,
            automaticallyImplyLeading: false,
            forceElevated: innerBoxIsScrolled,
            elevation: 5,
            pinned: false,
            primary: false,
            floating: true,
            centerTitle: true,
            expandedHeight: expandHeight < minFlexibleHeight ? minFlexibleHeight : expandHeight, //작업필요
            flexibleSpace: FlexibleSpaceBar(
              collapseMode: CollapseMode.pin,
              centerTitle: true,
              background: WMonthChart(year: widget.year, month: widget.month,),
            ),
            // bottom: PreferredSize(
            //   preferredSize: Size.fromHeight(30.0), // here the desired height
            //   child: WSearchHelper(streamController: _streamTextField,),
            // ),
          ),

        // new SliverFixedExtentList(
        //     itemExtent: 50.0,
        //     delegate: new SliverChildBuilderDelegate(
        //         (BuildContext context, int index) {
        //             return new Container(
        //                 alignment: Alignment.center,
        //                 color: Colors.lightBlue[100 * (index % 9)],
        //                 child: new Text('list item $index'),
        //             );
        //         },
        //     ),
        // ),

          SliverAppBar(
            // title: PreferredSize(
            //   preferredSize: Size.fromHeight(0.0), // here the desired height
            //   child: WSearchHelper(streamController: _streamTextField,),
            // ),
            title: WSearchHelper(streamController: _streamTextField,),
            backgroundColor: Color.fromARGB(255, 15, 15, 15),
          //  forceElevated: true,
            elevation: 10,
            titleSpacing: 0,
          //  automaticallyImplyLeading: true,
            floating: false,
            primary: false,
            pinned: true,
            expandedHeight: 0,
            // shape: RoundedRectangleBorder(
            //   borderRadius: new BorderRadius.circular(30.0)
            // ),
          ),

          //   title: WSearchHelper(streamController: _streamTextField,),
          //   backgroundColor: Color.fromARGB(255, 45, 45, 45),
          // ),
        ];
      },
      body: WAccountList(year: widget.year, month: widget.month, streamTextField: _streamTextField,),
    );
  }
}

class SearchView extends StatefulWidget {
  final int year;
  final int month;

  SearchView( {Key key, this.year=0, this.month=0} ) : super(key: key);
  @override State createState() => SearchViewState();
}

class SearchViewState extends State<SearchView> {
  StreamController  _streamTextField;
  ScrollController _scrollController;

  @override void initState() {
    super.initState();
    _streamTextField = new StreamController();
    _scrollController = new ScrollController();
    GlobalVariable.inst().pageDateTime = DateTime(widget.year, widget.month);
  }

  @override void deactivate() { 
    super.deactivate();
  }

  @override void didUpdateWidget(SearchView oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override void didChangeDependencies() { 
    super.didChangeDependencies();
  }

  @override void dispose() {
    _streamTextField.close();
    _scrollController.dispose();
    super.dispose();
  }

  @override Widget build(BuildContext context){
    return new NestedScrollView(
      scrollDirection: Axis.vertical,
      headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {  
        return <Widget>[
          SliverAppBar(
            title: WSearchHelper(streamController: _streamTextField,),
            backgroundColor: Color.fromARGB(255, 15, 15, 15),
            elevation: 10,
            titleSpacing: 0,
            floating: false,
            primary: false,
            pinned: true,
            expandedHeight: 0,
          ),
        ];
      },
      body: WAccountList(year: widget.year, month: widget.month, streamTextField: _streamTextField,),
    );
  }
}

class YearView extends StatefulWidget {
  final int year;
  YearView( {this.year} );

  @override State createState() => YearViewState();
}

class YearViewState extends State<YearView> with TickerProviderStateMixin{
  TabController     monthTabController;
  List<Tab>         _lstMonthTab;
  List<Widget>   _lstMonthTabView;

  @override void initState(){
    print("[YearView ${widget.year}] initState" + "---${widget.hashCode}----${n++}");
    super.initState();
    update();
    GlobalVariable.inst().getEvent("OnAddItem").register(onAddItem);
  }

  @override void didUpdateWidget(YearView oldWidget) {
    print("[YearView ${widget.year}] didUpdateWidget" + "---${widget.hashCode}----${n++}");
    super.didUpdateWidget(oldWidget);
    update();
  }

  @override void deactivate() { 
    print("[YearView ${widget.year}] deactivate" + "---${widget.hashCode}----${n++}");
    super.deactivate();
  }

  @override void didChangeDependencies() { 
    print("[YearView ${widget.year}] didChangeDependencies" + "---${widget.hashCode}----${n++}");
    super.didChangeDependencies();
  }

  @override void dispose(){
    print("[YearView ${widget.year}] dispose" + "---${widget.hashCode}-----------------------------------${n++}");
    monthTabController?.dispose();
    _lstMonthTab?.clear();
    _lstMonthTabView?.clear();
    GlobalVariable.inst().getEvent("OnAddItem").unregister(onAddItem);
    super.dispose();
  }

  void update(){
    print("[MonthView ${widget.year}] update" + "---${widget.hashCode}-----------------------------------${n++}");
    var lstMonth = UserData.inst().dataCache["list_month"][widget.year.toString()];
    
    if(lstMonth != null) {
      _lstMonthTab = List();
      _lstMonthTabView = List<Widget>();
      
      for(var month in lstMonth){
        _lstMonthTab.add(Tab(text: "- $month -",));
        _lstMonthTabView.add(MonthView(year: widget.year, month: month,));
      }

      _lstMonthTab.add(Tab(
        icon: Icon(
          Icons.search,
          color: Colors.blue,
          size: 20.0,
        ),
      ));
      _lstMonthTabView.add(SearchView(year: widget.year));

      _lstMonthTab.add(Tab(
        icon: Icon(
          Icons.insert_chart,
          color: Colors.blue,
          size: 20.0,
        ),
      ));
      _lstMonthTabView.add(WYearChart(year: widget.year));

      monthTabController = TabController(
        length: _lstMonthTab.length, 
        initialIndex: _lstMonthTab.length-3,
        vsync: this,
      );
    }
    else{
      monthTabController = null;
    }
  }

  @override Widget build(BuildContext context){
    print("[YearView ${widget.year}] build" + "---${widget.hashCode}-----------------------------------${n++}");

    return  Column(
      children: [
        Container(
          child: TabBar(
            controller: monthTabController,
            isScrollable: true,
            labelColor: Colors.white,
            unselectedLabelColor: Colors.white10,
            indicatorColor: Colors.white,
            indicatorWeight: 3,
            tabs: _lstMonthTab,
          ),
        ),
        Expanded(
          child: TabBarView(
            controller: monthTabController,
            children: _lstMonthTabView,
            physics: BouncingScrollPhysics(),
          ),
        ),
      ]
    );
  }

  void onAddItem(dynamic data){
  //  update();
  //  setState(() {});
    var month = data["month"];
    List lstMonth = UserData.inst().dataCache["list_month"][widget.year.toString()];
    for(int i=0; i<lstMonth.length; i++){
      if(lstMonth[i] == month){
        monthTabController.animateTo(i);
        break;
      }
    }
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);
  @override State createState() => MyHomePageState();
}

class MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin {
  TabController     _yearTabController;
  List <Tab>        _lstYearTab;
  List<Widget>    _lstYearTabView;
  StreamController  _eventStream;
  FocusNode _focusNode;
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override void initState(){
    print("[MyHomePage] initState" + "---${widget.hashCode}----${n++}");
    super.initState();
    _eventStream = GlobalVariable.inst().addStream("account_page");
    _focusNode = GlobalVariable.inst().addFocusNode("account_page");
    GlobalVariable.inst().createEvent("OnAccountPageTab");
    GlobalVariable.inst().createEvent("OnAddItem");

    _eventStream.stream.listen((event){
      switch(event){
        case "update":
          update();
          break;
        case "redraw":
          setState((){});
          break;
        default:
          print(event + "-- Event does not exists");
          break;
      }
    });
    update();
  }

  @override void deactivate() { 
    print("[MyHomePage] deactivate" + "---${widget.hashCode}----${n++}");
    super.deactivate();
  }

  @override void didUpdateWidget(MyHomePage oldWidget) {
    print("[MyHomePage] didUpdateWidget" + "---${widget.hashCode}----${n++}");
    super.didUpdateWidget(oldWidget);
  }

  @override void didChangeDependencies() { 
    print("[MyHomePage] didChangeDependencies" + "---${widget.hashCode}----${n++}");
    super.didChangeDependencies();
  }

  @override void dispose(){
    print("[MyHomePage] dispose" + "---${widget.hashCode}-----------------------------------${n++}");
    _yearTabController?.dispose();
    _lstYearTab?.clear();
    _lstYearTabView?.clear();
    GlobalVariable.inst().deleteStream("account_page");
    GlobalVariable.inst().deleteFocusNode("account_page");
    super.dispose();
  }

  void update(){
    print("[MyHomePage] update" + "---${widget.hashCode}-----------------------------------${n++}");

    var lstYear = UserData.inst().dataCache["list_year"];

    if(lstYear != null && lstYear.length > 0) {
      _lstYearTab = List();
      _lstYearTabView = List<Widget>();
      
      for(var year in lstYear){
        _lstYearTab.add(Tab(text: "- $year -",));
        _lstYearTabView.add(YearView(year: year));
      }

      _lstYearTab.add(Tab(text: "- CHART -",));
      _lstYearTabView.add(Container(color: Colors.red));

      _yearTabController = TabController(
        length: _lstYearTab.length, 
        initialIndex: _lstYearTab.length-2, 
        vsync: this
      );
    }
    else{
      _yearTabController = null;
    }
  }

//Set State 호출시 BUild 함수가 빠르게 호출되도록 최적화 되어있음.
  @override Widget build(BuildContext context) {
    print("[MyHomePage] build" + "---${widget.hashCode}-----------------------------------${n++}");

    final availableWidth = MediaQuery.of(context).size.width - 160;
    
    return GestureDetector(
      child: Scaffold(
        key: _scaffoldKey,
      //  extendBody: true,
    //  resizeToAvoidBottomInset : true,
    //  resizeToAvoidBottomPadding: true,
        body: buildYearView(),
        bottomNavigationBar: buildBottomAppBar(),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: new FloatingActionButton(
          onPressed: () => BottomModal.mainBottomSheet(context, _eventStream),
          backgroundColor: Color.fromARGB(255, 30, 30, 30),
          foregroundColor: Colors.white,
          highlightElevation: 1,
          isExtended: true,
          elevation: 4,
          child: Icon(Icons.panorama_fish_eye),
        ),
      ),
      behavior: HitTestBehavior.opaque,
      onTap: (){
        FocusScope.of(context).requestFocus(_focusNode);
        GlobalVariable.inst().getEvent("OnAccountPageTab").send("");
      },
    );
  }

  Widget buildYearView(){
    return _yearTabController!=null ? Column(
      children: [
        AppBar(
          titleSpacing: 0,
          centerTitle: true,
          title: TabBar(
            controller: _yearTabController,
            isScrollable: true,
            labelColor: Colors.white,
            unselectedLabelColor: Colors.white10,
            indicatorColor: Colors.white,
            indicatorWeight: 3,
            tabs: _lstYearTab,
          ),
        ),
        Expanded(
          child: TabBarView(
            controller: _yearTabController,
            children: _lstYearTabView,
          ),
        ),
      ]
    ) :
    Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("엉성한 가계Boo"),
      ),
      body: Center(
        child: CustumButton(
          iconSize: 100,
          width: 200,
          shape: CircleBorder(),
          foregroundColor: Color.fromARGB(255, 255, 255, 255),
          backgroundColor: Color.fromARGB(255, 30, 30, 30),
          iconData: Icons.add,
          onPressed: addItem,
        )
      )
    );
  }

  Widget buildBottomAppBar(){
    return BottomAppBar(
      color: Color.fromARGB(255, 30, 30, 30),
      notchMargin: 10.0,
      shape: CircularNotchedRectangle(),
      child: Row(
        mainAxisSize: MainAxisSize.max,
      //  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          WSpacer(width:5, color:Colors.transparent),
          CustumButton(
            iconSize: 20,
            width: 40,
            shape: CircleBorder(),
            foregroundColor: Color.fromARGB(255, 255, 255, 255),
            backgroundColor: Color.fromARGB(255, 40, 40, 40),
            iconData: Icons.add,
            onPressed: addItem,
          ),
          WSpacer(width:5, color:Colors.transparent),
          CustumButton(
            iconSize: 20,
            width: 40,
            shape: CircleBorder(),
            foregroundColor: Color.fromARGB(255, 255, 255, 255),
            backgroundColor: Color.fromARGB(255, 40, 40, 40),
            iconData: Icons.add,
            onPressed: (){ 
              setState((){}); 
            },
          ),
        ],
      ),
    );
  }

  void addItem() async {
    var item = await showDialog(
      context: context,
      builder: (content) => WAccountAddDialog(date: GlobalVariable.inst().pageDateTime),
    );

    if(item != null){
      var date = item["date"];
      await UserData.inst().addItem(data: item);
      update();
      setState(() {});

      await Future.delayed(Duration(milliseconds: 500));

      print("Add");
      List lstYear = UserData.inst().dataCache["list_year"];
      for(int i=0; i<lstYear.length; i++){
        if(lstYear[i] == date.year){
          _yearTabController.animateTo(i);
          break;
        }
      }
      GlobalVariable.inst().getEvent("OnAddItem").send({"month": date.month});

    //  var scaffold = Scaffold.of(context, nullOk: true);
    //  scaffold?.showSnackBar(
      _scaffoldKey.currentState.showSnackBar(
        SnackBar(
          duration: Duration(milliseconds: 1000),
          content: Row(
            children: <Widget>[
              Icon(
                Icons.check_circle_outline,
                size: 24,
              ),
              SizedBox(width: 8),
              Expanded(
                child: Text("새로운 정보 추가됨")
              )
            ],
          )
        )
      );
    }
  }

  void addDumpItem() async{
    var lstCategory = ["식사", "통신", "간식", "교통", "기타", "계발", "미분류"];

    for(int i=0 ; i<1000 ; ++i){
      var rand = randomBetween(0, 7);

      var rdmYear = randomBetween(2011, 2012);
      var rdmMonth = randomBetween(1, 13);
      var rdmDay = randomBetween(1, 32);
      String rdmCategory = lstCategory[rand];
      int rdmCost = randomBetween(1000, 30000);

      var item = <String, dynamic>{ 
        "date": new DateTime(rdmYear, rdmMonth, rdmDay), 
        "category": rdmCategory,
        "memo": randomString(40), 
        "cost": rdmCost,
      };
      if(i % 1000 == 0)
        print(i);
      await UserData.inst().addItem(data: item, isCreateCache: false);
    }
    await UserData.inst().createDataCache();

    update();
    setState(() {});
  }
 
  void clearList() async{
    await UserData.inst().clearAccount();
    update();
    setState(() {});
  }

  void openData() async{

  }

  void saveData() async{
  
  }
}


class BottomModal{
  static void mainBottomSheet(BuildContext context, StreamController eventStream){
    showModalBottomSheet(
      context: context,
      builder: (BuildContext context){
        return Container(
          height: 300,
          color: Colors.white10,
          child: Column(
            children: <Widget>[
              _createTile(context, "테이블 삭제", Icons.message, () async{
                await UserData.inst().clearAccount();
                eventStream.add("update");
                eventStream.add("redraw");
              }),
              _createTile(context, "테이블 값 삽입", Icons.message, () async{
                await UserData.inst().openDataFromJson();
                eventStream.add("update");
                eventStream.add("redraw");
              }),
              // _createTile(context, "테이블 출력", Icons.message, () async{
              //   await UserData.inst().queryAccount("""SELECT * FROM AccountData ORDER BY year, month, day, idx""");
              // }),
              _createTile(context, "엑셀에서 데이터 가져오기", Icons.message, () async{
                await UserData.inst().openDataFromExcel();
                eventStream.add("update");
                eventStream.add("redraw");
              }),    
            ],
          )
        );
      }
    );
  }

  static ListTile _createTile(BuildContext context, String name, IconData icon, Function action){
    return ListTile(
      leading: Icon(icon),
      title: Text(name),
      onTap: (){
        Navigator.pop(context);
        action();
      }
    );
  }
}




        // appBar: AppBar(
        //   centerTitle: true,
        //   title: buildYearButton(),
        //   actions: <Widget>[
        //     CustumButton(
        //       iconSize: 20,
        //       width: 40,
        //       shape: CircleBorder(),
        //       foregroundColor: Color.fromARGB(255, 255, 255, 255),
        //       iconData: Icons.account_circle,
        //       onPressed: () {
        //         Navigator.pushNamed(context, "/sign_in");
        //       },
        //     ),
        //     CustumButton(
        //       iconSize: 20,
        //       width: 40,
        //       shape: CircleBorder(),
        //       foregroundColor: Color.fromARGB(255, 255, 255, 255),
        //       iconData: Icons.settings,
        //       onPressed: () {
        //         Navigator.pushNamed(context, "/option");
        //       },
        //     ),
        //   ],
        // ),
      //  endDrawer: Text("asdfasdf"),


// bottom: TabBar(
//   key: _monthTabKey,
//   controller: _monthTabController,
//   isScrollable: true,
//   labelColor: Color.fromARGB(255, 0, 255, 0),
//   unselectedLabelColor: Color.fromARGB(50, 0, 255, 0),
//   indicatorColor: Color.fromARGB(255, 0, 255, 0),
//   indicatorWeight: 3,
//   tabs: _lstMonthTab,
// ),


  // persistentFooterButtons: <Widget>[
  //   IconButton(
  //     icon: new Icon(Icons.add),
  //     onPressed: (){},
  //   ),
  //   IconButton(
  //     icon: new Icon(Icons.add),
  //     onPressed: (){},
  //   ),
  //   IconButton(
  //     icon: new Icon(Icons.add),
  //     onPressed: (){},
  //   ),
  //   IconButton(
  //     icon: new Icon(Icons.add),
  //     onPressed: (){},
  //   ),
  // ],
  //bottomSheet: Text("asdfasdfasdf"),




// //    for(int i=0 ; i<_lstMonthTab.length ; ++i)
//     _containers = List();
//     account_year.forEach((k, v){
//       _containers.add(
//         CustomScrollView(
//           shrinkWrap: false,
//           primary: true,
//           slivers: <Widget>[
//             SliverAppBar(
//              // titleSpacing: 100,
//               centerTitle: false, 
//               backgroundColor: Color.fromARGB(255, 30, 30, 30),
//               expandedHeight: 260,
//               automaticallyImplyLeading: false,
//               elevation: 10,
//               snap: true,
//               forceElevated: true,
//               floating: true,
//               flexibleSpace: FlexibleSpaceBar(
//                 centerTitle: true,
//                 collapseMode: CollapseMode.parallax,
//             //    titlePadding: EdgeInsetsGeometry(),
//              //   title: Text("Text"),
//                 background: AccountChart(year, int.parse(k)),
//               ),
//             ),
//             WAccountList(year, int.parse(k)),
//           ],
//         )
//       );
//     });


//    for(int i=0 ; i<_lstMonthTab.length ; ++i)


// title: Text(widget.title),
// title: Container (
// //  color: Colors.black,
//   child: TabBar(
//     isScrollable: true,
//     tabs: _lstTab,
//     labelColor: Color.fromARGB(255, 255, 255, 255),
//     unselectedLabelColor: Color.fromARGB(255, 0, 0, 0),
//     indicatorColor: Color.fromARGB(255, 255, 255, 255),
//     indicatorWeight: 5,
//   ),
// ),

//  _containers = List();
// _containers.add(ListView.builder(
//     itemBuilder: (_, int index)=>EachList(_lstName[index]),
//     itemCount: this._lstName.length,
//   )
// );

  //  _lstTab = List();
    // _lstTab.add(Tab(text: "2021",));
    // _lstTab.add(Tab(text: "2022",));
    // _lstTab.add(Tab(text: "2023",));
    // _lstTab.add(Tab(text: "2024",));
    // _lstTab.add(Tab(text: "2025",));
    // _lstTab.add(Tab(text: "2026",));
    // _lstTab.add(Tab(text: "2027",));
    // _lstTab.add(Tab(text: "2028",));
    
    //var w = WAccountList(UserData.inst().lstAccount);
    //w.
    //w.getSize();
    // GlobalKey g = GlobalKey();
    // w.key = g;
    // MediaQuery.of(context).size.width


  
            // SliverAppBar(
            //   expandedHeight: 40.0 * 6,
            //   floating: true,
            //   pinned: false,
            //   primary: true,
            //   flexibleSpace: FlexibleSpaceBar(
            //     centerTitle: true,
            //     collapseMode: CollapseMode.parallax,
            // //    titlePadding: EdgeInsetsGeometry(),
            //     background:  TabBarView(
            //       controller: _tabController,
            //       children: _containers,
            //     ),
            //   ),
            // ),

          

          // SliverFillViewport(
          //   delegate: SliverChildListDelegate(
          //     [
          //       Container(color: Colors.yellow),
          //     ],
          //   ),
          // ),


          // SliverFixedExtentList(
          //   itemExtent: 40.0 * (_containers.length+1),
          //   delegate: SliverChildListDelegate(
          //     [
          //       TabBarView(
          //         controller: _tabController,
          //         children: _containers,
          //       ),
          //     ],
          //   ),
          // ),

            // SliverFillRemaining(
            //   child: TabBarView(
            //     controller: _tabController,
            //     children: container2,
            //   )
            // ),
        //   WAccountList(2019, 5),



                      // flexibleSpace: FlexibleSpaceBar(
              //   centerTitle: true,
              //   collapseMode: CollapseMode.parallax,
              //   background:  TabBar(
              //     controller: _tabController,
              //     isScrollable: true,
              //     labelColor: Color.fromARGB(255, 0, 255, 0),
              //     unselectedLabelColor: Color.fromARGB(50, 0, 255, 0),
              //     indicatorColor: Color.fromARGB(255, 0, 255, 0),
              //     indicatorWeight: 3,
              //     tabs: _lstMonthTab,
              //   ),
              // ),

                 // appBar: AppBar(
      //   centerTitle: false,
      //  // toolbarOpacity: 0,
      //   backgroundColor: Color.fromARGB(255, 30, 30, 30),
      // //  title: Text(widget.title),
      //   // title: TabBar(
      //   //   controller: _tabController,
      //   //   isScrollable: true,
      //   //   labelColor: Color.fromARGB(255, 0, 255, 0),
      //   //   unselectedLabelColor: Color.fromARGB(50, 0, 255, 0),
      //   //   indicatorColor: Color.fromARGB(255, 0, 255, 0),
      //   //   indicatorWeight: 3,
      //   //   tabs: _lstMonthTab,
      //   // ),
      //   // leading: IconButton(
      //   //   icon: new Icon(Icons.add),
      //   //   color: Colors.red,
      //   //   onPressed: (){},
      //   // ),
        
      //   flexibleSpace: FlexibleSpaceBar(
      //     centerTitle: true,
      //     collapseMode: CollapseMode.parallax,
      // //    titlePadding: EdgeInsetsGeometry(),
      //     title: Text("Text"),
      //     background: TabBar(
      //       controller: _tabController,
      //       isScrollable: true,
      //       labelColor: Color.fromARGB(255, 0, 255, 0),
      //       unselectedLabelColor: Color.fromARGB(50, 0, 255, 0),
      //       indicatorColor: Color.fromARGB(255, 0, 255, 0),
      //       indicatorWeight: 3,
      //       tabs: _lstMonthTab,
      //     ),
      //   ),

      //   actions: [], 
      // ),
      // body: TabBarView(
      //   controller: _tabController,
      //   children: _containers,
      // ),

      // appBar: AppBar(
      //   backgroundColor: Color.fromARGB(255, 30, 30, 30),
      // //  title: Text(widget.title),
      //   title: TabBar(
      //     controller: _tabController,
      //     isScrollable: true,
      //     labelColor: Color.fromARGB(255, 0, 255, 0),
      //     unselectedLabelColor: Color.fromARGB(50, 0, 255, 0),
      //     indicatorColor: Color.fromARGB(255, 0, 255, 0),
      //     indicatorWeight: 3,
      //     tabs: _lstMonthTab,
      //   ),
      // ),




            // body: NestedScrollView(
      //   controller: _scrollController,
      //   headerSliverBuilder: (BuildContext context, bool boxIsScrolled) { 
      //     return <Widget>[
      //       // SliverAppBar(
      //       //   backgroundColor: Color.fromARGB(255, 30, 30, 30),
      //       //   expandedHeight: 60,
      //       // //  primary: true,
      //       // //  floating: true,
      //       // //  pinned: true,
      //       //   title: Align(
      //       //     child: FlatButton(
      //       //       child: Text("$_year", 
      //       //         style: TextStyle(
      //       //           color: Colors.green,
      //       //           fontSize: 20,
      //       //         ),
      //       //       ),
      //       //       onPressed: changeYear,
      //       //     ),
      //       //   ),
      //       // ),

      //       // SliverAppBar(
      //       // //  snap: true,
      //       // //  pinned: true,
      //       // //  primary: true,
      //       //   floating: true,
      //       //   expandedHeight: 60,
      //       //   title: TabBar(
      //       //     key: _monthTabKey,
      //       //     controller: _monthTabController,
      //       //   //  isScrollable: true,
      //       //     labelColor: Color.fromARGB(255, 0, 255, 0),
      //       //     unselectedLabelColor: Color.fromARGB(50, 0, 255, 0),
      //       //     indicatorColor: Color.fromARGB(255, 0, 255, 0),
      //       //     indicatorWeight: 3,
      //       //     tabs: _lstMonthTab,
      //       //   ),
      //       // ),
      //     ];
      //   },
      //   body: TabBarView(
      //     //physics: AlwaysScrollableScrollPhysics(),
      //   //  physics: NeverScrollableScrollPhysics(),
      //     controller: _monthTabController,
      //     children: _lstTabView,
      //   ),
      // ),


        // Widget _buildActions() {
  //   Widget profile = new GestureDetector(
  //     onTap: () { print("aaaaaaaaaaaaaaa"); },
  //     child: new Container(
  //       height: 30.0,
  //       width: 45.0,
  //       decoration: new BoxDecoration(
  //         shape: BoxShape.circle,
  //         color: Colors.blue,
  //       //  image: new DecorationImage(
  //       //  //  image: new ExactAssetImage("assets/logo.png"),
  //       //    fit: BoxFit.cover,
  //       //  ),
  //         border: Border.all(color: Colors.black, width: 2.0),
  //       ),
  //     ),
  //   );

  // print(_scrollController.offset.toString() + "-------------------");

  //   double scale;
  //   if (_scrollController.hasClients) {
  //     scale = _scrollController.offset / 300;
  //     scale = scale * 2;
  //     if (scale > 1) 
  //     {
  //       scale = 1.0;
  //     }
  //   } 
  //   else 
  //   {
  //     scale = 0.0;
  //   }

  //   return new Transform(
  //     transform: new Matrix4.identity()..scale(scale, scale),
  //     alignment: Alignment.center,
  //     child: profile,
  //   );
  // }

