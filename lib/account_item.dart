import 'dart:math';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:numberpicker/numberpicker.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter/material.dart' as prefix0;
import 'package:pie_chart/pie_chart.dart';
import 'package:intl/intl.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_test_00/account_widget/date_divider.dart';
import 'custum_wiget_tool.dart';
import 'user_data.dart';

//Intl.defaultLocale = 'pt_BR';

class WCategory extends StatelessWidget{
  final String _category;
  Icon _icon;
  bool isChart;
  
  WCategory(this._category, { this.isChart = false });

  @override Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Container(
          width: 30,
          height: 30,
          decoration: BoxDecoration(
            color: Colors.black26,
            borderRadius: BorderRadius.circular(45),
          ),
          child: Icon(
            Icons.fastfood, 
          //  FontAwesomeIcons.gamepad,
            size: 20,
            color: UserData.inst().getCategoryColor(_category),
          ),
        ),
        WSpacer(width:5, color:Colors.transparent),
        Container(
          decoration: BoxDecoration(
            color: UserData.inst().getCategoryColor(_category),
            borderRadius: (isChart) ? BorderRadius.only(topLeft: const Radius.circular(5.0), bottomLeft: const Radius.circular(5.0)) : 
                                      BorderRadius.all(const Radius.circular(5.0))
          ),
          margin: const EdgeInsets.only(right: 5.0),
          width: 45,
          height: 40,
          child: Align(
            alignment: Alignment.center, 
            child: BorderedText(
              strokeWidth: 1,
              child: Text(_category,
                style: TextStyle(
                  fontSize: 10, 
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class WAccountAddBar extends StatefulWidget {
  WAccountAddBar({Key key}) : super(key: key);

  @override WAccountAddBarState createState(){
    return WAccountAddBarState();
  }
}

class WAccountAddBarState extends State<WAccountAddBar> {
  String _currentItem = "식사";
  String _inputFieldValue = "메모";

  void onChanged (String value){
    setState(() {
      _currentItem = value;
    });
  }

  @override Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(const Radius.circular(100.0)),
        color: Colors.black12,
      ),
      height: 30,
      child: Row(
        children: [
          // Expanded(
          //   flex: 5,
          //   child: Container(
          //     color: Colors.black38,
          //     child: TextFormField(
          //       initialValue: "0",
          //       textAlign: TextAlign.right,
          //       keyboardType: TextInputType.datetime,
          //       style: TextStyle(
          //         fontSize: 10,
          //         color: Colors.white,
          //       ),
          //       decoration: InputDecoration(
          //       //  labelStyle: TextStyle(
          //       //    color: Colors.white,
          //       //  ),
          //       ),
          //       onEditingComplete: null,
          //       onFieldSubmitted: (value){
          //         SystemChrome.setEnabledSystemUIOverlays([]);
          //       },
          //       inputFormatters: [
          //         CurrencyFormat(),
          //       ],
          //     ),
          //   ),
          // ),
          // WSpacer(width: 5,),
          Container(
            color: Colors.black38,
            width: 115,
            child: DropdownButtonHideUnderline(
              child: ButtonTheme(
                alignedDropdown: false,
              // color: Colors.white,
                child:  DropdownButton(
                  iconDisabledColor: Colors.black38,
                  isDense: true,
                  isExpanded: true,
                  value: _currentItem,
                  hint: Text("Hint"),
                  iconSize: 30,
                  iconEnabledColor: Colors.white,
                  items: [
                    DropdownMenuItem(value: "식사", child: WCategory("식사")),
                    DropdownMenuItem(value: "통신", child: WCategory("통신")),
                    DropdownMenuItem(value: "간식", child: WCategory("간식")),
                    DropdownMenuItem(value: "교통", child: WCategory("교통")),
                    DropdownMenuItem(value: "기타", child: WCategory("기타")),
                    DropdownMenuItem(value: "계발", child: WCategory("계발")),
                  ],
                  onChanged: onChanged,
                ),
              ),
            ),
          ),
          WSpacer(width: 5),
          Expanded(
            flex: 10,
            child: Container(
              color: Colors.black38,
              child: TextFormField(
                initialValue: _inputFieldValue,
                textInputAction: TextInputAction.done,
                keyboardAppearance: Brightness.dark,
                maxLength:10,
                maxLines: 1,
              //  autofocus: true,
              //  textDirection: prefix0.TextDirection.rtl,
              //  autofocus: true,
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontSize: 12,
                  color: Colors.white,
                ),
                decoration: InputDecoration(
                  counterText: "",
                  counterStyle: TextStyle(
                    color: Colors.white,
                  ),
                //  labelText: "- 메모 -",
                //  labelStyle: TextStyle(
                //    color: Colors.white,
                //  ),
                ),
                onFieldSubmitted: (value){
                  setState(() {
                    _inputFieldValue = value;
                    if(_inputFieldValue.isEmpty == true){
                      _inputFieldValue = "메모";
                    }
           //         SystemChrome.setEnabledSystemUIOverlays([]);

                  });
                },
                onEditingComplete: null,
                inputFormatters: [
                //  LengthLimitingTextInputFormatter(1),
                ],
              ),
            ),
          ),
          WSpacer(width: 5,),
          Expanded(
            flex: 5,
            child: Container(
              color: Colors.black38,
              child: TextFormField(
                initialValue: "0",
                textAlign: TextAlign.right,
                keyboardType: TextInputType.number,
                style: TextStyle(
                  fontSize: 10,
                  color: Colors.white,
                ),
                decoration: InputDecoration(
                //  labelStyle: TextStyle(
                //    color: Colors.white,
                //  ),
                ),
                onEditingComplete: null,
                onFieldSubmitted: (value){
         //         SystemChrome.setEnabledSystemUIOverlays([]);
                },
                inputFormatters: [
                  CurrencyFormat(),
                ],
              ),
            ),
          ),
          WSpacer(width: 5,),
          Container(
            width: 30,
            height: 30,
            decoration: BoxDecoration(
              color: Color.fromARGB(100, 20, 20, 20),
              borderRadius: BorderRadius.all(const Radius.circular(45.0)),
            ),
          //  color: Colors.white10,
            child: Align(
              alignment: Alignment.center,
              child: Text("(₩)",
                style: TextStyle(
                  fontSize: 10, 
                  fontWeight: FontWeight.bold,
                  color: Colors.green,
                ),
              ),
            )
          ),
        ],
      ),
    );
  }
}

class WAccountAddDialog extends StatefulWidget {
  WAccountAddDialog({Key key}) : super(key: key);

  @override WAccountAddDialogState createState(){
    return WAccountAddDialogState();
  }
}

class WAccountAddDialogState extends State<WAccountAddDialog> {
  DateTime _dateTime;
  String _category;
  var _ctrlMemoField = TextEditingController();
  var _ctrlCostField = TextEditingController();
  

@override void initState() {
    super.initState();
    _dateTime = DateTime.now();
    _category = "식사";
    _ctrlMemoField.text = "";
    _ctrlCostField.text = "0";
  }

  Future selectDate(BuildContext context) async{

    var date = DateTime.now();

    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: date,
      firstDate: DateTime(2000),
      lastDate: DateTime(2020),
    );

    if(picked != null && picked != date){
      _dateTime = picked;
    }

    setState(() {});
  }

  @override Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: SimpleDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(const Radius.circular(30.0))
        ),
        backgroundColor: Color.fromARGB(255, 50, 50, 50),
        elevation: 10,
        title: Container(
          child: Align(
            child: Text("항목 추가",
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          ),
        ),
        children: <Widget>[
          WSpacer(height: 50),
            Container(
            margin: const EdgeInsets.only(left: 10.0, right: 10),
            decoration: BoxDecoration(
              color: Colors.white12,
              borderRadius: BorderRadius.all(const Radius.circular(10.0)),
            ),
            child: Row(
              children: [
                Container(
                  margin: const EdgeInsets.only(left: 5.0),
                  width: 60,
                  child: Align(
                    child: BorderedText(
                      strokeColor: Colors.black,
                      strokeWidth: 1,
                      child: Text("날       짜 : ",
                        style: TextStyle(
                          fontSize: 12, 
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ),
                WSpacer(width: 15),
                CustumButton(
                  iconSize: 15,
                  width: 30,
                  shape: CircleBorder(),
                  backgroundColor: Color.fromARGB(30, 255, 255, 255),
                  foregroundColor: Color.fromARGB(255, 255, 255, 255),
                  iconData: Icons.remove,
                  onPressed: () {
                    _dateTime = _dateTime.add(Duration(days: -1));
                    setState(() {});
                  },
                ),
                  Expanded(
                  child: InkWell(
                    child: Container(
                      height: 50,
                      child: Align(
                        child: Text(gdate1Formatter.format(_dateTime)),       
                      ),
                    ),
                    onTap: (){
                      selectDate(context);
                    },
                  ),
                ),
                  CustumButton(
                  iconSize: 15,
                  width: 30,
                  shape: CircleBorder(),
                  backgroundColor: Color.fromARGB(30, 255, 255, 255),
                  foregroundColor: Color.fromARGB(255, 255, 255, 255),
                  iconData: Icons.add,
                  onPressed: () {
                    _dateTime = _dateTime.add(Duration(days: 1));
                    setState(() {});
                  },
                ),
                WSpacer(width: 15),
                // WSpacer(width: 10),              
                // CustumButton(
                //   shape: CircleBorder(),
                //   foregroundColor: Color.fromARGB(255, 0, 255, 0),
                //   backgroundColor: Color.fromARGB(255, 40, 40, 40),
                //   iconData: Icons.calendar_today,
                //   onPressed: () {
                //     selectDate(context);
                //   },
                // ),
              ]
            ),
          ),
            WSpacer(height: 10),
            Container(
            margin: const EdgeInsets.only(left: 10.0, right: 10),
            decoration: BoxDecoration(
              color: Colors.white12,
              borderRadius: BorderRadius.all(const Radius.circular(10.0)),
            ),
            child: Row(
              children: [
                Container(
                  margin: const EdgeInsets.only(left: 5.0),
                  width: 60,
                  child: Align(
                    child: BorderedText(
                      strokeWidth: 1,
                      child: Text("카테고리 : ",
                        style: TextStyle(
                          fontSize: 12, 
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ),
                  Expanded(
                  child: Container(
                    margin: const EdgeInsets.only(left: 50.0, right: 50),
                    child: DropdownButtonHideUnderline(
                      child: ButtonTheme(
                        alignedDropdown: false,
                      // color: Colors.white,
                        child:  DropdownButton(
                          iconDisabledColor: Colors.black38,
                          isDense: true,
                          isExpanded: true,
                          iconSize: 30,
                          iconEnabledColor: Colors.white,
                          value: _category,
                          items: [
                            DropdownMenuItem(value: "식사", child: WCategory("식사")),
                            DropdownMenuItem(value: "통신", child: WCategory("통신")),
                            DropdownMenuItem(value: "간식", child: WCategory("간식")),
                            DropdownMenuItem(value: "교통", child: WCategory("교통")),
                            DropdownMenuItem(value: "기타", child: WCategory("기타")),
                            DropdownMenuItem(value: "계발", child: WCategory("계발")),
                          ],
                          onChanged: (value){
                            _category = value;
                            setState(() {});
                          },
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
            
          WSpacer(height: 10),
            Container(
            margin: const EdgeInsets.only(left: 10.0, right: 10),
            decoration: BoxDecoration(
              color: Colors.white12,
              borderRadius: BorderRadius.all(const Radius.circular(10.0)),
            ),
            child: Row(
              children: [
                Container(
                  margin: const EdgeInsets.only(left: 5.0),
                  width: 60,
                  child: Align(
                    child: BorderedText(
                      strokeWidth: 1,
                      child: Text("  메       모 : ",
                        style: TextStyle(
                          fontSize: 12, 
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    
                    child: TextFormField(
                      controller: _ctrlMemoField,
                      textInputAction: TextInputAction.done,
                      keyboardAppearance: Brightness.dark,
                      maxLength:35,
                      maxLines: 1,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 12,
                        color: Colors.white,
                      ),
                      decoration: InputDecoration(
                      //  counterText: "",
                        counterStyle: TextStyle(
                          color: Colors.white30,
                        ),
                      //  labelText: "- 메모 -",
                      //  labelStyle: TextStyle(
                      //    color: Colors.white,
                      //  ),
                      ),
                      onFieldSubmitted: (value){
                        setState(() {
                      //    _inputFieldValue = value;
                      //    if(_inputFieldValue.isEmpty == true){
                      //      _inputFieldValue = "메모";
                      //    }
                 //         SystemChrome.setEnabledSystemUIOverlays([]);
                          });
                      },
                      
                      onEditingComplete: null,
                      inputFormatters: [
                      //  LengthLimitingTextInputFormatter(1),
                      ],
                    ),
                  ),
                ),
                WSpacer(width: 35),    
              ]
            ),
          ),
            WSpacer(height: 10),
            Container(
            margin: const EdgeInsets.only(left: 10.0, right: 10),
            decoration: BoxDecoration(
              color: Colors.white12,
              borderRadius: BorderRadius.all(const Radius.circular(10.0)),
            ),
            child: Row(
              children: [
                Container(
                  margin: const EdgeInsets.only(left: 5.0),
                  width: 60,
                  child: Align(
                    child: BorderedText(
                      strokeWidth: 1,
                      child: Text("  금       액 : ",
                        style: TextStyle(
                          fontSize: 12, 
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                  //  color: Colors.black38,
                    child: TextFormField(
                      controller: _ctrlCostField,
                      textAlign: TextAlign.center,
                      keyboardType: TextInputType.number,
                      style: TextStyle(
                        fontSize: 10,
                        color: Colors.white,
                      ),
                      decoration: InputDecoration(
                       labelStyle: TextStyle(
                         color: Colors.white,
                       ),
                      ),
                      onEditingComplete: (){},
                      onFieldSubmitted: (value){
            //            SystemChrome.setEnabledSystemUIOverlays([]);
                      },
                      inputFormatters: [
                        CurrencyFormat(),
                      ],
                    ),
                  ),
                ),
                  WSpacer(width: 5),
                
                Container(
                  width: 30,
                  height: 30,
                  decoration: BoxDecoration(
                    color: Color.fromARGB(100, 20, 20, 20),
                    borderRadius: BorderRadius.all(const Radius.circular(45.0)),
                  ),
                //  color: Colors.white10,
                  child: Align(
                    alignment: Alignment.center,
                    child: Text("(₩)",
                      style: TextStyle(
                        fontSize: 10, 
                        fontWeight: FontWeight.bold,
                        color: Colors.green,
                      ),
                    ),
                  )
                ),
              ]
            ),
          ),
            WSpacer(height: 50),
            Container(
            child: Row(
              children: [
                WSpacer(width: 50),
                Container(
                  decoration: BoxDecoration(
                    color: Colors.red,
                    borderRadius: BorderRadius.all(const Radius.circular(10.0)),
                  ),
                  child: SimpleDialogOption(
                    child: Text('취   소'),
                    onPressed: (){
                      Navigator.pop(context, null);
             //         SystemChrome.setEnabledSystemUIOverlays([]);
                    },
                  ),
                ),
                Expanded(
                  child: Container(
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.all(const Radius.circular(10.0)),
                  ),
                  child: SimpleDialogOption(
                    child: Text('추   가'),
                    onPressed: (){
                      Navigator.pop(context, <String, dynamic>{ "date": _dateTime, "category": _category,"memo": _ctrlMemoField.text.toString(), "cost": int.parse(_ctrlCostField.text.replaceAll(RegExp('[\,]'), ''))});
             //         SystemChrome.setEnabledSystemUIOverlays([]);
                    },
                  ),
                ),
                WSpacer(width: 50,),
              ]
            ),
          ),
        ],
      )
    );
  }
}


RegExp regexPrice = RegExp(r"[^\d]");
class CurrencyFormat extends TextInputFormatter {
  @override TextEditingValue formatEditUpdate( TextEditingValue oldValue, TextEditingValue newValue) {
    if (newValue.text.length > 14) {
      return oldValue;
    }
    if (newValue.text.trim() == "\$") {
      return newValue.copyWith(text: " ");
    }
    else {
      int num = int.parse(newValue.text.replaceAll(regexPrice, ''));

      final newString = gCostFormatter.format(num);
      return TextEditingValue(
        selection: TextSelection.fromPosition(
          TextPosition(offset: newString.length)
        ),
        text: newString
      );
    }
  }
}

class WCost extends StatelessWidget{
  int     _cost = 0;
  double  _width = 100;

  WCost({int cost, double width}){
    _cost = cost;
    _width = width;
  }

  @override Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
       // color: Color.fromARGB(50, 0, 0, 0),
       // borderRadius: BorderRadius.all(const Radius.circular(45.0)),
      ), 
      height: 35,
      margin: const EdgeInsets.all(2.0),
      child: Row(
        children: <Widget>[ 
          Container(
            width: _width,
            decoration: BoxDecoration(
              color: Color.fromARGB(100, 20, 20, 20),
              borderRadius: BorderRadius.all(const Radius.circular(5.0)),
            ),
            child: Align(
              alignment: Alignment(0.9, 0),
              child: Text(gCostFormatter.format(_cost),
                style: TextStyle(
                  fontSize: 10, 
                  color: Colors.white,
                ),
              ),
            ),
          ),
          WSpacer(width: 3),
          Container(
            width: 30,
            height: 30,
            decoration: BoxDecoration(
              color: Color.fromARGB(100, 20, 20, 20),
              borderRadius: BorderRadius.all(const Radius.circular(45.0)),
            ),
          //  color: Colors.white10,
            child: Align(
              alignment: Alignment.center,
              child: Text("(₩)",
                style: TextStyle(
                  fontSize: 10, 
                  fontWeight: FontWeight.bold,
                  color: Colors.green,
                ),
              ),
            )
          ),
        ],
      ),
    );
  }
}

class WCategoryCost extends StatelessWidget{
  String _category = "";
  int    _cost = 0;
  int   _sumCost;
  int   _leftPadding;

  WCategoryCost({dynamic data}){
    _category = data["category"];
    _cost = data["cost"];
    _sumCost = data["sumCost"];
    _leftPadding = data["leftPadding"];
  }

  @override Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Color.fromARGB(50, 0, 0, 0),
        borderRadius: BorderRadius.all(const Radius.circular(45.0)),
      ), 
      height: 35,
     // width: 350,
      margin: const EdgeInsets.only(left:10.0, right: 10.0, top:2, bottom:2),
      child: Row(
        children: <Widget>[ 
        //  WSpacer(width:5),
          WCategory(_category, isChart: true,),
          Expanded(
            flex: 25,
            child: Container(
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.white54,
                ),
                color: Color.fromARGB(100, 20, 20, 20),
                borderRadius: BorderRadius.only(topRight: const Radius.circular(5.0), bottomRight: const Radius.circular(5.0)),
              ),
              child: Stack(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Expanded(
                        flex: _leftPadding.toInt(),
                        child: Container(),
                      ),
                      Expanded(
                        flex: _cost,
                        child: Container(
                          decoration: BoxDecoration(
                            color: UserData.inst().getCategoryColor(_category),
                            border: Border.all(
                              color: Colors.white70,
                            ),
                            borderRadius: (_leftPadding!=0) ? BorderRadius.all(const Radius.circular(5.0)) :
                                                             BorderRadius.only(topRight: const Radius.circular(5.0), bottomRight: const Radius.circular(5.0)),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: (_sumCost - _cost - _leftPadding).toInt(),
                        child: Container(),
                      ),
                    ],
                  ),
                  Positioned(
                    child: Align(
                      alignment: Alignment(0, 0),
                      child: BorderedText(
                        strokeWidth: 1,
                        child: Text(gPercentFormatter.format(_cost/_sumCost * 100) + " %",
                          style: TextStyle(
                            fontSize: 10, 
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),

          WSpacer(width:3, color:Colors.transparent),

          WCost(cost: _cost, width: 80),
        ],
      ),
    );
  }
}

class AccountChart extends StatefulWidget {
  int _year;
  int _month;
  Widget _widget;

  AccountChart({Key key, int year, int month}) : super(key: key) {
    _year = year;
    _month = month;

    _widget = FutureBuilder(
      future: UserData.inst().queryAccount(
        """SELECT category, cost FROM AccountData 
          WHERE 
            year == $_year AND 
            month == $_month
        """), // a previously-obtained Future<String> or null
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
            return Container();
          case ConnectionState.active:
          case ConnectionState.waiting:
            return SpinKitCircle(
              color: Colors.white,
              size: 100.0,
            );
          case ConnectionState.done:
            if (snapshot.hasError)
                return Text('Error: ${snapshot.error}');
            var lstAccount = snapshot.data;

            //Add All Category
            var categoryCost = Map<String, double>();
            UserData.inst().categoryData.forEach((k, v) {
              categoryCost[k] = 0;
            });

            int sumCost = 0;
            for (var item in lstAccount){
              categoryCost[item.category] += item.cost.toDouble();
              sumCost += item.cost;
            }

            //Remove 0 Valued Category
            //categoryCost.removeWhere((k, v) {return v == 0.0; });

            int leftPadding = 0;
            var lstWidget = List<Widget>();
            categoryCost.forEach((k, v){
              lstWidget.add(WCategoryCost( data: <String, dynamic>{ "category": k, "cost": v.toInt(), "leftPadding": leftPadding, "sumCost": sumCost } ));
              leftPadding += v.toInt();
            });

            return Container(
              height: lstWidget.length * 40.0,
              child: Column(
                children: <Widget>[
                  Expanded(
                    flex: 10,
                    child: Column(
                      children: lstWidget,
                    ),
                  ),
                ],
              ),
            );
        }
        return Container();
      },
    );
  }

  @override AccountChartState createState() => AccountChartState();
}

class AccountChartState extends State<AccountChart>{
  @override Widget build(BuildContext context) => widget._widget;
}

class WAccountItem extends StatelessWidget{
  String _category = "";
  String _memo = "";
  int    _cost = 0;

  WAccountItem({dynamic data}){
    _category = data.category;
    _memo = data.memo;
    _cost = data.cost;
  }

  @override Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Color.fromARGB(50, 0, 0, 0),
        borderRadius: BorderRadius.all(const Radius.circular(45.0)),
      ), 
      height: 25,
      margin: const EdgeInsets.only(left: 10, right: 10, bottom:5),
    //  margin: const EdgeInsets.all(5.0),
      child: Row(
      //  mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[ 
          WCategory(_category),
          Expanded(
            flex: 70,
            child: Container(
              decoration: BoxDecoration(
                color: Color.fromARGB(100, 20, 20, 20),
                borderRadius: BorderRadius.all(const  Radius.circular(5.0)),
              ),
            //  color: Colors.white10,
              child: Align(
                alignment: Alignment(-0.9, 0), 
                child: Text(_memo,
                  style: TextStyle(
                    fontSize: 10, 
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ),
          WSpacer(width:3),
          WCost(cost: _cost, width: 80),
        ],
      ),
    );
  }
}

class WAccountList extends StatelessWidget{
  final int _year;
  final int _month;

  WAccountList(this._year, this._month);

  @override Widget build(BuildContext context) {
    return FutureBuilder(
      future: UserData.inst().queryAccount(
        """SELECT * FROM AccountData 
          WHERE 
            year == $_year AND 
            month == $_month
        """), // a previously-obtained Future<String> or null
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
            return SliverList(
          delegate: SliverChildListDelegate([]),
        );
          case ConnectionState.active:
          case ConnectionState.waiting:
            // return SliverAppBar(
            //   flexibleSpace: SpinKitFoldingCube (
            //       color: Colors.white,
            //       size: 10.0,
            //     ),
            // );
            return SliverToBoxAdapter(
              child: Container(
                color: Colors.red,
                child: SpinKitFoldingCube (
                  color: Colors.white,
                  size: 100.0,
                ),
              ),
            );
          case ConnectionState.done:
            if (snapshot.hasError)
                return Text('Error: ${snapshot.error}');
            var lstAccount = snapshot.data;

            var lstAccountWidget = List<Widget>();
            DateTime pvtDate;

            lstAccountWidget.add(
              Container(
                color: Colors.white,
                height: 0.5,
                margin: const EdgeInsets.only(left:20.0, right:20.0, bottom:10.0),
              ),
            );

         //   lstAccountWidget.add(WAccountAddBar());
            for (var item in lstAccount) {
              DateTime time = DateTime(_year, _month, item.day);
              if(pvtDate != time){
                pvtDate = time;
                lstAccountWidget.add(WDateDivider(date: pvtDate));
              }
              lstAccountWidget.add(WAccountItem(data: item));
            }
            lstAccountWidget.add(Container(height: 30,));

            return SliverList(
              delegate: SliverChildBuilderDelegate(
                (context, index){
                  return lstAccountWidget[index];
                },
                childCount: lstAccountWidget.length,
              ),
            );
        }
        return SliverList(
          delegate: SliverChildListDelegate([]),
        );
      },
    );
  }
}



// class AccountPieChart extends StatelessWidget{
//   final dynamic _categoryCost;
//   GlobalKey _containerKey = GlobalKey();

//   int an;

//   AccountPieChart(this._categoryCost);

//   Size getSize(){
//     final RenderBox containerRenderBox = _containerKey.currentContext.findRenderObject();
//     final containerSize = containerRenderBox.size;
//     return containerSize;
//   }

//   @override Widget build(BuildContext context) {
//     if(_categoryCost == null)
//       return Container();

//    // print(getSize().width.toString() + " - " + getSize().height.toString());

//     var lstWidget = List<Widget>();
//     _categoryCost.forEach((k, v){
//       lstWidget.add(WCategoryCost( data: <String, dynamic>{ "category": k, "cost": v.toInt(), } ),);
//     });

//     return Container(
//       key: _containerKey,
//       decoration: BoxDecoration(
//         gradient: LinearGradient(
//           begin: Alignment.topCenter,
//           end: Alignment.bottomCenter,
//           colors: [
//             Color.fromARGB(255, 100, 100, 100),
//             Color.fromARGB(255, 45, 45, 45),
//           ],
//         ),
//       ),   
//       child: Column(
//         children: <Widget>[
//         //  WSpacer(height: 25),
//           // Expanded(
//           //   flex: 20,
//           //   child: PieChart(
//           //     chartRadius: MediaQuery.of(context).size.width / 1.5,
//           //     animationDuration: Duration(milliseconds: 1000),
//           //     dataMap: _categoryCost,
//           //     legendFontColor: Colors.white,
//           //     legendFontSize: 14.0,
//           //     legendFontWeight: FontWeight.w100,
//           //     chartLegendSpacing: 50.0,
//           //     showLegends: false,
//           //     showChartValuesInPercentage: true,
//           //     showChartValues: true,
//           //     showChartValuesOutside: true,
//           //     chartValuesColor: Colors.white,
//           //   //  chartValuesColor: Colors.blueGrey[900].withOpacity(0.9),
//           //     colorList: glstColor,
//           //   )
//           // ),
//           Expanded(
//             flex: 10,
//             child: Column(
//               children: lstWidget,
//             ),
//           ),
//           Container(
//             color: Colors.white,
//             height: 0.5,
//             margin: const EdgeInsets.only(left:20.0, right:20.0, bottom:5.0),
//           ),
//         ],
//       ),
//     );
//   }
// }


// class WAccountList extends StatelessWidget{
//   final dynamic _lstAccountData;

//   WAccountList(this._lstAccountData);

//   @override Widget build(BuildContext context) {
//     if(_lstAccountData == null)
//       return SliverAppBar();

//     var lstAccountWidget = List<Widget>();
//     DateTime pvtDate;

//     lstAccountWidget.add(
//       Container(
//         color: Colors.white,
//         height: 0.5,
//         margin: const EdgeInsets.only(left:20.0, right:20.0, bottom:10.0),
//       ),
//     );

//     for (var item in _lstAccountData) {
//       DateTime time = DateTime(item["time"][0], item["time"][1], item["time"][2]);
//       if(pvtDate != time){
//         pvtDate = time;
//         lstAccountWidget.add(WDateDivider(date: pvtDate));
//       }
//       lstAccountWidget.add(WAccountItem(data: item));
//     }

//     return SliverList(
//       delegate: SliverChildBuilderDelegate(
//         (context, index){
//           return lstAccountWidget[index];
//         },
//         childCount: lstAccountWidget.length,
//       ),
//     );
//   }
// }

// SizedBox(
// width: 100.0,
//   child: DropdownButtonFormField<int>(
//     items: <DropdownMenuItem<int>>[
//       DropdownMenuItem<int>(
//         value: 1,
//         child: Text("Owner"),
//       ),
//       DropdownMenuItem<int>(
//         value: 2,
//         child: Text("Member"),
//       ),
//     ],
//     decoration: InputDecoration.collapsed(hintText: '', fillColor: Colors.black),
//     value: 2,
//   ),
// ),

// GestureDetector(
//   onTap: () {
//     print("onTap called.");
//   },
//   child: Text("foo"),
// ),