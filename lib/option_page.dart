import 'package:flutter/material.dart';

class OptionPage extends StatelessWidget {
  @override Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromARGB(255, 30, 30, 30),
        title: Text("Option Page", ),
      ),
      body: Center(
        child: RaisedButton(
          color: Color.fromARGB(255,  30, 30, 30),
          child: Text("Back",
            style: TextStyle(color: Colors.white),
          ),
          onPressed:(){
            Navigator.pop(context);
          }
        ),
      ),
    );
  }
}
