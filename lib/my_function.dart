
DateTime intToDateTime(int date){
  int year = (date ~/ 10000);
  int month = ((date ~/ 100) % 100);
  int day = date % 100;
  return DateTime(year, month, day);
}

int dateTimeToInt(DateTime dateTime){
  return (dateTime.year * 10000) + (dateTime.month * 100) + dateTime.day;
}