import 'package:flutter/material.dart';

class CircleImage extends StatelessWidget {
 final String renderUrl;

 CircleImage(this.renderUrl);

 @override
 Widget build(BuildContext context) {
   return Container(
    width: 50.0,
    height: 50.0,
    decoration: BoxDecoration(
      shape: BoxShape.circle,
      image: DecorationImage(
        fit: BoxFit.cover,
        image: NetworkImage(renderUrl ?? ''),
      ),
    ),
  );
 }
}

class EachList extends StatelessWidget{
  final String name;
  EachList(this.name);

  @override
  Widget build(BuildContext context){
  //  String nowTime = DateTime.now().toString();
  //String nowTime = "-";

    return Card(
      margin: EdgeInsets.only(left: 5.0, right: 5.0, top: 5.0, bottom: 5.0,),
      elevation: 10,
    //  borderOnForeground: false,
      semanticContainer: false,
      color: Color.fromARGB(255, 30, 30, 30), 
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),// + Border.all(color: Color.fromARGB(255, 0, 255, 0)),

      child: InkWell(
        splashColor: Color.fromARGB(255, 0, 255, 0),
        radius: 1000,
        onTap: (){
        },
        child: Container(
          height: 60,
          child: Row(
            children: <Widget>[
              Padding(padding: EdgeInsets.only(right: 10.0)),
              CircleAvatar(
                minRadius: 10.0,
                maxRadius: 20.0,
          //      radius: 25.0,
                child: Icon(Icons.fastfood),
                foregroundColor: Colors.white,
                backgroundColor: Colors.cyan,
            ),
              // CircleImage(
              //   "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTbISHdEyuY-ii2Ekeexb3AJOaPfbdcuvRKGRS1X6WOlac2SzUflA"
              // ),
              Padding(padding: EdgeInsets.only(right: 10.0)),
              Divider(color: Colors.white, indent: 5),
              Text(
                name, 
                style: TextStyle(
                  fontSize: 10, 
                  color: Colors.white,
                ),
                softWrap: true,
              ),
         //     TextField(
         //       decoration: InputDecoration(
         //         border: InputBorder.none,
         //         hintText: 'Enter a search term'
         //       ),
         //     ),
            ],
          ),
        )
      ),
    );
  }
}
