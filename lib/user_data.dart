import 'dart:convert'; //to convert json to maps and vice versa
import 'dart:io';
import 'dart:async';
import 'dart:math' show Random;
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart'; //add path provider dart plugin on pubspec.yaml file
import 'package:random_string/random_string.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:simple_permissions/simple_permissions.dart';
import 'package:flutter_file_manager/flutter_file_manager.dart';
import 'package:spreadsheet_decoder/spreadsheet_decoder.dart';

import 'my_function.dart';
import 'custum_wiget_tool.dart';
import 'account_widget/category.dart';

class AccountData{
  int key;
  int year;
  int month;
  int day;
  int idx;
  String category;
  String memo;
  int cost;

  AccountData({int key=0, int year, int month, int day, int idx, String category, String memo, int cost})
  {
    this.key = key;
    this.year = year;
    this.month = month;
    this.day = day;
    this.idx = idx;
    this.category = category;
    this.memo = memo;
    this.cost = cost;
  }

  factory AccountData.fromJson(Map<String, dynamic> data) => new AccountData(
    key: data["key"],
    year: data["year"],
    month: data["month"],
    day: data["day"],
    idx: data["idx"],
    category: data["category"],
    memo: data["memo"],
    cost: data["cost"],
  );

  Map<String, dynamic> toMap() => {
    "year": year,
    "month": month,
    "day": day,
    "idx": idx,
    "category": category,
    "memo": memo,
    "cost": cost,
  };

  void printData(){
    print("$key -- " + "$year -- " + "$month -- " + "$day -- " + "$idx -- " + "$category -- " + "$memo -- " + "$cost");
  }
}

class UserData{
  static UserData _instance;
  Database _database;
  dynamic _dataCache = Map<String, dynamic>();
  dynamic get dataCache => _dataCache;

  static UserData inst(){
    if(UserData._instance == null){
      UserData._instance = UserData();
    }
    return UserData._instance;
  }

  Future initDatabase() async {
  //  String path = await getDatabasesPath();
  //  String dbPath = join(databasesPath, 'my.db');

  // if(await databaseExists(dbPath) == true) {
  //   await deleteDatabase(dbPath);
  // }

    PermissionStatus permissionResult = await SimplePermissions.requestPermission(Permission.WriteExternalStorage);
    if (permissionResult == PermissionStatus.authorized){
      var path = await getExternalStorageDirectory();
      String dbPath = join(join(path.path, "Roughly_Account_Book"), "database.db");


      if(_database != null)
        _database.close();

      _database = await openDatabase(dbPath, 
        version: 2, 
        onCreate: (Database database, int version) async {
          //key        BIGINT PRIMARY KEY identity(1,1),
          await database.execute(
            """CREATE TABLE AccountData (
              key        INTEGER PRIMARY KEY AUTOINCREMENT,
              year       INTEGER,
              month      INTEGER,
              day        INTEGER,
              idx        INTEGER,
              category   TEXT,
              memo       TEXT,
              cost       INTEGER
            )"""
          );
        },
      );

      await createDataCache();
    }
  }

  Future createDataCache() async{
    //DB에 존재하는 년도 List 생성
    var queryResult = await _database.rawQuery("""SELECT DISTINCT year FROM AccountData ORDER BY year""");
    var lstYear = new List();
    for(var year in queryResult)
      lstYear.add(year["year"]);
    _dataCache["list_year"] = lstYear;

    //DB에 존재하는 달 List 생성
    _dataCache["list_month"] = Map<String, dynamic>();
    for(var year in _dataCache["list_year"]){
      var queryResult = await _database.rawQuery("""SELECT DISTINCT month FROM AccountData WHERE year == $year ORDER BY month""");
      List lstMonth = new List();
      for(var month in queryResult)
        lstMonth.add(month["month"]);
      _dataCache["list_month"][year.toString()] = lstMonth;
    }
  }

  Future addItem({dynamic data, bool isCreateCache = true}) async{
    var item = <String, dynamic>{
      "year": data["date"].year, 
      "month": data["date"].month, 
      "day": data["date"].day, 
      "category": data["category"], 
      "memo": data["memo"], 
      "cost": data["cost"] 
    };

    var lstData = await _database.rawQuery(
      """SELECT * FROM AccountData 
        WHERE 
          year == ${item["year"]} AND 
          month == ${item["month"]} AND
          day == ${item["day"]}
      """
    );
    
    //마지막 index 추출
    item["idx"] = lstData != 0 ? lstData.length : 0;
    await _database.insert("AccountData", item);
  
    if(isCreateCache == true)
      await createDataCache();
  }

  Future deleteItem({dynamic data, bool isCreateCache = true}) async {
    await _database.delete("AccountData", where: "key == ?", whereArgs: [data["key"]]);

    if(isCreateCache == true)
      await createDataCache();
  }

  Future<List<AccountData>> queryAccount(String query) async {
  //  await Future.delayed(const Duration(milliseconds: 1000), () => "1");
    List<Map> list = await _database.rawQuery(query);
    List<AccountData> lstAccountData = new List();
    for (int i = 0; i < list.length; i++) {
      var data = AccountData.fromJson(list[i]);
      lstAccountData.add(data);
      //data.printData();
    }
    return lstAccountData;
  }

  Future clearAccount() async{
    if(_database != null){
        await _database.rawQuery("DELETE FROM AccountData");
    await createDataCache();
    }
  }

  Future openDataFromJson() async{
    var json = jsonDecode(accountData);
    Map account_data = json["account_data"];

    for(var year in account_data.entries){
      Map account_year = account_data[year.key];
      for (var entry in account_year.entries) {
        for(var data in entry.value) {
          var newData = <String, dynamic>{
            "date": DateTime(data["year"], data["month"], data["day"]), 
            "category": data["category"], 
            "memo": data["memo"], 
            "cost": data["cost"] 
          };
          await addItem(data: newData, isCreateCache: false); 
        }    
      }
    }
    await createDataCache();
  }

  Future openDataFromExcel() async{
    PermissionStatus permissionResult = await SimplePermissions.requestPermission(Permission. WriteExternalStorage);
    if (permissionResult == PermissionStatus.authorized){
      var externalDir = await getExternalStorageDirectory();
      String path = join(join(externalDir.path, "Roughly_Account_Book"), "input_data.xlsx");
      //String path = join('assets', "input_data2.xlsm");

      var excelBytes = new File(path).readAsBytesSync();
      var decoder = new SpreadsheetDecoder.decodeBytes(excelBytes);

      var lst = decoder.tables.values.toList();
      for(var table in lst){
        DateTime preTime;
        for(int i = 1 ; i<table.maxRows ; i++){
          var row = table.rows[i];

          String time = row[0];
          DateTime pvtTime;
          if(time != '▷'){
            time = time.replaceAll(new RegExp('/'), '-');
            time = time.substring(0, 10);
            pvtTime = DateTime.parse(time);
            preTime = pvtTime;
          }
          else{
            pvtTime = preTime;
          }

          double cost = gCostFormatter.parse(row[3].substring(0, row[3].length-4));
          String category = row[1].toString();
          category = categoryData.containsKey(category) ? category : "미분류";

          var newData = <String, dynamic>{
            "date": DateTime(pvtTime.year, pvtTime.month, pvtTime.day), 
            "category": category, 
            "memo": row[2].toString(), 
            "cost": cost, 
          };
          await addItem(data: newData, isCreateCache: false); 
        }
      }
    }
    await createDataCache();
  }

  Future saveData() async{
    
  }

  List createCategoryMenuItem({bool isItem = true}){
    List lstCategoryMenuItem = new List<DropdownMenuItem<String>>();

    for(String key in categoryData.keys.toList()){
      if(isItem == true){
        if(key == "전체")
          continue;
      }
      lstCategoryMenuItem.add(DropdownMenuItem(value: key, child: WCategory(mCategory: key)));
    }

    return lstCategoryMenuItem;
  }




    var accountData = """
    {
      "account_data": {
        "2018": {
          "1": [
            {          "year": 2018, "month": 1, "day": 3,          "category": "통신",          "memo": "d버거슈퍼 맛있는 와플",          "cost": 14300        },
            {          "year": 2018, "month": 1, "day": 5,          "category": "식사",          "memo": "e부라라라누누",       "cost": 10000        }
          ],
          "2": [
            {          "year": 2018, "month": 2, "day": 1,          "category": "식사",          "memo": "홍대 돈부리",                      "cost": 90700        },
            {          "year": 2018, "month": 2, "day": 2,          "category": "교통",          "memo": "w구란다란디라다",          "cost": 63000        },
            {          "year": 2018, "month": 2, "day": 5,          "category": "식사",          "memo": "e부라라라누누",       "cost": 10000        },
            {          "year": 2018, "month": 2, "day": 6,          "category": "간식",          "memo": "a살사쏘쓰맨",                      "cost": 6240        },
            {          "year": 2018, "month": 2, "day": 7,          "category": "계발",          "memo": "qwerqwer",          "cost": 12300        }
          ],
          "5": [
            {          "year": 2018, "month": 5, "day": 5,          "category": "식사",          "memo": "e부라라라누누",       "cost": 10000        },
            {          "year": 2018, "month": 5, "day": 6,          "category": "간식",          "memo": "a살사쏘쓰맨",                      "cost": 6240        },
            {          "year": 2018, "month": 5, "day": 7,          "category": "계발",          "memo": "qwerqwer",          "cost": 12300        }
          ],
          "6": [
            {          "year": 2018, "month": 6, "day": 1,          "category": "식사",          "memo": "홍대 돈부리",                      "cost": 90700        },
            {          "year": 2018, "month": 6, "day": 2,          "category": "교통",          "memo": "w구란디 다란디라다",          "cost": 630000        },
            {          "year": 2018, "month": 6, "day": 2,          "category": "식사",          "memo": "b울랄라라라",          "cost": 5670        },
            {          "year": 2018, "month": 6, "day": 7,          "category": "계발",          "memo": "ㅈㄷㄹㅈㄷ",          "cost": 3400        },
            {          "year": 2018, "month": 6, "day": 7,          "category": "계발",          "memo": "qwerqwer",          "cost": 12300        }
          ],
          "7": [
            {          "year": 2018, "month": 7, "day": 1,          "category": "식사",          "memo": "홍대 돈부리",                      "cost": 90700        },
            {          "year": 2018, "month": 7, "day": 7,          "category": "계발",          "memo": "qwerqwer",          "cost": 12300        }
          ]
        },
        "2019": {
          "1": [
            {          "year": 2019, "month": 1, "day": 3,          "category": "통신",          "memo": "d버거슈퍼 맛있는 와플",          "cost": 14300        },
            {          "year": 2019, "month": 1, "day": 5,          "category": "식사",          "memo": "e부라라라누누",       "cost": 10000        },
            {          "year": 2019, "month": 1, "day": 6,          "category": "간식",          "memo": "a살사쏘쓰맨",                      "cost": 6240        },
            {          "year": 2019, "month": 1, "day": 7,          "category": "계발",          "memo": "qwerqwer",          "cost": 12300        }
          ],
          "2": [
            {          "year": 2019, "month": 2, "day": 1,          "category": "식사",          "memo": "홍대 돈부리",                      "cost": 90700        },
            {          "year": 2019, "month": 2, "day": 1,          "category": "통신",          "memo": "d버거슈퍼 맛있는 와플",          "cost": 14300        },
            {          "year": 2019, "month": 2, "day": 1,          "category": "간식",          "memo": "a살사쓰맨",                      "cost": 6240        },
            {          "year": 2019, "month": 2, "day": 2,          "category": "교통",          "memo": "w구란다란디라다",          "cost": 63000        },
            {          "year": 2019, "month": 2, "day": 2,          "category": "기타",          "memo": "c부라라누누",          "cost": 47650        },
            {          "year": 2019, "month": 2, "day": 5,          "category": "식사",          "memo": "e부라라라누누",       "cost": 10000        },
            {          "year": 2019, "month": 2, "day": 6,          "category": "간식",          "memo": "a살사쏘쓰맨",                      "cost": 6240        },
            {          "year": 2019, "month": 2, "day": 6,          "category": "간식",          "memo": "a살사쏘쓰맨",                      "cost": 6240        },
            {          "year": 2019, "month": 2, "day": 7,          "category": "계발",          "memo": "qwerqwer",          "cost": 12300        }
          ],
          "5": [
            {          "year": 2019, "month": 5, "day": 1,          "category": "식사",          "memo": "홍대 돈부리",                      "cost": 90700        },
            {          "year": 2019, "month": 5, "day": 1,          "category": "통신",          "memo": "d버거슈퍼 맛있는 와플",          "cost": 14300        },
            {          "year": 2019, "month": 5, "day": 1,          "category": "간식",          "memo": "a살사쓰맨",                      "cost": 6240        },
            {          "year": 2019, "month": 5, "day": 2,          "category": "교통",          "memo": "w구란다란디라다",          "cost": 63000        },
            {          "year": 2019, "month": 5, "day": 2,          "category": "기타",          "memo": "c부라라누누",          "cost": 47650        },
            {          "year": 2019, "month": 5, "day": 2,          "category": "식사",          "memo": "b울랄라라",          "cost": 5670        },
            {          "year": 2019, "month": 5, "day": 2,          "category": "식사",          "memo": "b울랄라라",          "cost": 5670        },
            {          "year": 2019, "month": 5, "day": 4,          "category": "식사",          "memo": "다란다",          "cost": 55440        },
            {          "year": 2019, "month": 5, "day": 4,          "category": "식사",          "memo": "w구란디 다란디라다",   "cost": 100        },
            {          "year": 2019, "month": 5, "day": 5,          "category": "식사",          "memo": "e부라라라누누",       "cost": 10000        },
            {          "year": 2019, "month": 5, "day": 5,          "category": "계발",          "memo": "ㅈㄷㄹㅈㄷ",          "cost": 10000        },
            {          "year": 2019, "month": 5, "day": 6,          "category": "간식",          "memo": "a살사쏘쓰맨",                      "cost": 6240        },
            {          "year": 2019, "month": 5, "day": 6,          "category": "간식",          "memo": "a살사쏘쓰맨",                      "cost": 6240        },
            {          "year": 2019, "month": 5, "day": 7,          "category": "계발",          "memo": "ㅈㄷㄹㅈㄷ",          "cost": 3400        },
            {          "year": 2019, "month": 5, "day": 7,          "category": "계발",          "memo": "ㅈㄷㄹㅈㄷ",          "cost": 3400        },
            {          "year": 2019, "month": 5, "day": 7,          "category": "계발",          "memo": "ㅈㄷㄹㅈㄷ",          "cost": 3400        },
            {          "year": 2019, "month": 5, "day": 7,          "category": "계발",          "memo": "ㅈㄷㄹㅈㄷ",          "cost": 3400        },
            {          "year": 2019, "month": 5, "day": 7,          "category": "계발",          "memo": "ㅈㄷㄹㅈㄷ",          "cost": 3400        },
            {          "year": 2019, "month": 5, "day": 7,          "category": "계발",          "memo": "ㅈㄷㄹㅈㄷ",          "cost": 3400        },
            {          "year": 2019, "month": 5, "day": 7,          "category": "계발",          "memo": "ㅈㄷㄹㅈㄷ",          "cost": 3400        },
            {          "year": 2019, "month": 5, "day": 7,          "category": "계발",          "memo": "ㅈㄷㄹㅈㄷ",          "cost": 3400        },
            {          "year": 2019, "month": 5, "day": 7,          "category": "계발",          "memo": "ㅈㄷㄹㅈㄷ",          "cost": 3400        },
            {          "year": 2019, "month": 5, "day": 7,          "category": "계발",          "memo": "ㅈㄷㄹㅈㄷ",          "cost": 3400        },
            {          "year": 2019, "month": 5, "day": 7,          "category": "계발",          "memo": "ㅈㄷㄹㅈㄷ",          "cost": 3400        },
            {          "year": 2019, "month": 5, "day": 7,          "category": "계발",          "memo": "ㅈㄷㄹㅈㄷ",          "cost": 3400        },
            {          "year": 2019, "month": 5, "day": 7,          "category": "계발",          "memo": "ㅈㄷㄹㅈㄷ",          "cost": 3400        },
            {          "year": 2019, "month": 5, "day": 7,          "category": "계발",          "memo": "ㅈㄷㄹㅈㄷ",          "cost": 3400        },
            {          "year": 2019, "month": 5, "day": 7,          "category": "계발",          "memo": "ㅈㄷㄹㅈㄷ",          "cost": 3400        },
            {          "year": 2019, "month": 5, "day": 7,          "category": "계발",          "memo": "ㅈㄷㄹㅈㄷ",          "cost": 3400        },
            {          "year": 2019, "month": 5, "day": 7,          "category": "계발",          "memo": "ㅈㄷㄹㅈㄷ",          "cost": 3400        },
            {          "year": 2019, "month": 5, "day": 7,          "category": "계발",          "memo": "ㅈㄷㄹㅈㄷ",          "cost": 3400        },
            {          "year": 2019, "month": 5, "day": 7,          "category": "계발",          "memo": "ㅈㄷㄹㅈㄷ",          "cost": 3400        },
            {          "year": 2019, "month": 5, "day": 7,          "category": "계발",          "memo": "ㅈㄷㄹㅈㄷ",          "cost": 3400        },
            {          "year": 2019, "month": 5, "day": 7,          "category": "계발",          "memo": "qwerqwer",          "cost": 12300        }
          ],
          "6": [
            {          "year": 2019, "month": 6, "day": 1,          "category": "식사",          "memo": "홍대 돈부리",                      "cost": 90700        },
            {          "year": 2019, "month": 6, "day": 2,          "category": "교통",          "memo": "w구란디 다란디라다",          "cost": 630000        },
            {          "year": 2019, "month": 6, "day": 2,          "category": "기타",          "memo": "c부라라라누누",          "cost": 47650        },
            {          "year": 2019, "month": 6, "day": 2,          "category": "식사",          "memo": "b울랄라라라",          "cost": 5670        },
            {          "year": 2019, "month": 6, "day": 2,          "category": "식사",          "memo": "b울랄라라라",          "cost": 5670        },
            {          "year": 2019, "month": 6, "day": 4,          "category": "식사",          "memo": "다란디라다",          "cost": 55440        },
            {          "year": 2019, "month": 6, "day": 4,          "category": "식사",          "memo": "w구란디 다란디라다",   "cost": 100        },
            {          "year": 2019, "month": 6, "day": 5,          "category": "식사",          "memo": "e부라라라누누",       "cost": 10000        },
            {          "year": 2019, "month": 6, "day": 5,          "category": "계발",          "memo": "ㅈㄷㄹㅈㄷ",          "cost": 10000        },
            {          "year": 2019, "month": 6, "day": 6,          "category": "간식",          "memo": "a살사쏘쓰맨",                      "cost": 6240        },
            {          "year": 2019, "month": 6, "day": 6,          "category": "간식",          "memo": "a살사쏘쓰맨",                      "cost": 6240        },
            {          "year": 2019, "month": 6, "day": 7,          "category": "계발",          "memo": "ㅈㄷㄹㅈㄷ",          "cost": 3400        },
            {          "year": 2019, "month": 6, "day": 7,          "category": "계발",          "memo": "ㅈㄷㄹㅈㄷ",          "cost": 3400        },
            {          "year": 2019, "month": 6, "day": 7,          "category": "계발",          "memo": "qwerqwer",          "cost": 12300        }
          ],
          "7": [
            {          "year": 2019, "month": 7, "day": 1,          "category": "식사",          "memo": "홍대 돈부리",                      "cost": 90700        },
            {          "year": 2019, "month": 7, "day": 1,          "category": "통신",          "memo": "d버거킹 슈퍼 맛있는 와플",          "cost": 14300        },
            {          "year": 2019, "month": 7, "day": 7,          "category": "계발",          "memo": "ㅈㄷㄹㅈㄷ",          "cost": 3400        },
            {          "year": 2019, "month": 7, "day": 7,          "category": "계발",          "memo": "ㅈㄷㄹㅈㄷ",          "cost": 3400        },
            {          "year": 2019, "month": 7, "day": 7,          "category": "계발",          "memo": "qwerqwer",          "cost": 12300        }
          ],
          "8": [
            {          "year": 2019, "month": 8, "day": 1,          "category": "통신",          "memo": "d버거킹 슈퍼 맛있는 와플",          "cost": 14300        }
          ],
          "9": [
            {          "year": 2019, "month": 9, "day": 1,          "category": "통신",          "memo": "d버거킹 슈퍼 맛있는 와플",          "cost": 14300        }
          ],
          "10": [
            {          "year": 2019, "month": 10, "day": 1,          "category": "통신",          "memo": "d버거킹 슈퍼 맛있는 와플",          "cost": 14300        }
          ],
          "11": [
            {          "year": 2019, "month": 11, "day": 1,          "category": "통신",          "memo": "d버거킹 슈퍼 맛있는 와플",          "cost": 14300        }
          ]
        }
      }
    }
    """;

  var lstDistinctColor = [
    Color.fromARGB(255, 100, 100, 100),
    Color.fromARGB(255, 230, 25, 75), 
    Color.fromARGB(255, 60, 180, 75), 
    Color.fromARGB(255, 255, 225, 25), 
    Color.fromARGB(255, 0, 130, 200), 
    Color.fromARGB(255, 245, 130, 48), 
    Color.fromARGB(255, 145, 30, 180), 
    Color.fromARGB(255, 70, 240, 240), 
    Color.fromARGB(255, 240, 50, 230), 
    Color.fromARGB(255, 210, 245, 60), 
    Color.fromARGB(255, 250, 190, 190), 
    Color.fromARGB(255, 0, 128, 128), 
    Color.fromARGB(255, 230, 190, 255), 
    Color.fromARGB(255, 170, 110, 40), 
    Color.fromARGB(255, 255, 250, 200), 
    Color.fromARGB(255, 128, 0, 0), 
    Color.fromARGB(255, 170, 255, 195), 
    Color.fromARGB(255, 128, 128, 0), 
    Color.fromARGB(255, 255, 215, 180), 
    Color.fromARGB(255, 0, 0, 128), 
    Color.fromARGB(255, 128, 128, 128), 
    Color.fromARGB(255, 255, 255, 255), 
    Color.fromARGB(255, 0, 0, 0)
  ];

  Map categoryData = <String, dynamic>{
    "전체": <String, dynamic>   { "index": 0, "category": "전체", "icon":null, }, 
    "식사": <String, dynamic>   { "index": 1, "category": "식사", "icon":null, },
    "간식": <String, dynamic>   { "index": 2, "category": "간식", "icon":null, }, 
    "통신": <String, dynamic>   { "index": 3, "category": "통신",  "icon":null, }, 
    "교통": <String, dynamic>   { "index": 4, "category": "교통", "icon":null, }, 
    "계발": <String, dynamic>   { "index": 5, "category": "계발",  "icon":null, }, 
    "유흥": <String, dynamic>   { "index": 6, "category": "유흥",  "icon":null, }, 
    "비품": <String, dynamic>   { "index": 7, "category": "비품",  "icon":null, },
    "연례행사": <String, dynamic>   { "index": 8, "category": "연례행사",  "icon":null, }, 
    "미분류": <String, dynamic> { "index": 9, "category": "미분류", "icon":null, },
  };

  dynamic getCategoryData(String category){
    if(categoryData.containsKey(category) == true){
      return categoryData[category];
    }
    else{
      return categoryData["미분류"];
    }
  }

  Color getCategoryColor(String category){
    if(categoryData.containsKey(category) == true){
      return lstDistinctColor[categoryData[category]["index"]];
    }
    else{
      return lstDistinctColor[categoryData["미분류"]["index"]];
    }
  }
}

  // void populateDb(Database database, int version) async {
  //   // Batch batch = database.batch();
  //   // batch.execute("Your query-> Create table if not exists");
  //   // batch.execute("Your query->Create table if not exists");
  //   // List<dynamic> res = await batch.commit();
    
  //   //key        BIGINT PRIMARY KEY identity(1,1),
  //   await database.execute(
  //     """CREATE TABLE AccountData (
  //       key        INTEGER PRIMARY KEY AUTOINCREMENT,
  //       year       INTEGER,
  //       month      INTEGER,
  //       day        INTEGER,
  //       idx        INTEGER,
  //       category   TEXT,
  //       memo       TEXT,
  //       cost       INTEGER
  //     )"""
  //   );
  // }


  // void extractAccountData(){
  // //  lstAccount = jsonData["account_list"];
  //   lstAccount = List();
  //   categoryCost = Map<String, double>();
    
  //   var day = 5;
  //   var lstCategory = ["식사", "통신", "간식", "교통", "기타", "계발"];

  //   for(int i=0 ; i<500 ; ++i){
  //     var rand = randomBetween(0, 6);

  //     if(rand < 2)
  //       day++;

  //     String category = lstCategory[rand];
  //     int cost = randomBetween(0, 30000);

  //     lstAccount.add(<String, dynamic>{ "time": [2019, 5, day], "category": category, "memo": randomString(40), "cost": cost  });
  //   }

  //   for (var item in lstAccount){
  //     if(categoryCost.containsKey(item["category"]))
  //         categoryCost[item["category"]] += item["cost"].toDouble();
  //       else
  //         categoryCost[item["category"]] = item["cost"].toDouble();
  //   }
  // }

// getApplicationDocumentsDirectory().then((Directory directory) {
    //   File file = File(directory.path + "/" + fileName);
    //   file.createSync();
    //   file.writeAsStringSync(json.encode(accountData));

    //   // showDialog(
    //   // //  context: context,
    //   //   builder: (BuildContext context) {
    //   //     // return object of type Dialog
    //   //     return AlertDialog(
    //   //       title: Text("알림"),
    //   //       content: Text("데이터를 저장 하였습니다"),
    //   //       actions: <Widget>[
    //   //         // usually buttons at the bottom of the dialog
    //   //         FlatButton(
    //   //           child: Text("닫기"),
    //   //           onPressed: () {
    //   //             Navigator.of(context).pop();
    //   //           },
    //   //         ),
    //   //       ],
    //   //     );
    //   //   },
    //   // );
    // });

  // final Future database = openDatabase( 
  //   // join function을 이용하여 각 platform마다 올바르게 경로를 확인 
  //   path: join(await getDatabasePath(), 'example_database.db'), 
    
  //   // onCreate function을 실행하기 위한 version 설정 
  //   version: 1, 
    
  //   // create table 구문 실행 
  //   onCreate: (db, version) { 
  //     return db.execute( 
  //       "create table example(id integer, contets text)", 
  //     ); 
  //   }, 
  // ); 

  // Future initDB() async {
  //   var databasesPath = await getDatabasesPath();
  //   String path = join(databasesPath, 'demo.db');

  //   final Future database = openDatabase( 
  //     // join function을 이용하여 각 platform마다 올바르게 경로를 확인 
  //     path: join(await getDatabasePath(), 'example_database.db'), 
      
  //     // onCreate function을 실행하기 위한 version 설정 
  //     version: 1, 
      
  //     // create table 구문 실행 
  //     onCreate: (db, version) { 
  //       return db.execute( 
  //         "create table example(id integer, contets text)", 
  //       ); 
  //     }, 
  //   ); 


  //   Directory documentsDirectory = await getApplicationDocumentsDirectory();
  //   String path = join(documentsDirectory.path, "TestDB.db");
  //   return await openDatabase(path, version: 1, onOpen: (db) {
  //   }, onCreate: (Database db, int version) async {
  //     await db.execute("CREATE TABLE Client ("
  //         "id INTEGER PRIMARY KEY,"
  //         "first_name TEXT,"
  //         "last_name TEXT,"
  //         "blocked BIT"
  //         ")");
  //   });
  // }





  // Future saveData() async {    
  //   String fileName = "myJSONFile.json";
  //   var dir = await getApplicationDocumentsDirectory();

  //   if(dir != null){
  //     File file = File(dir.path + "/" + fileName);
  //     file.createSync();
  //     file.writeAsStringSync(json.encode(accountData));
  //   }
  // }





  // code of read or write file in external storage (SD card)
      
    //   //await _files();
    //   //await _getSpecificFileTypes();
    //   String databasesPath = await getDatabasesPath();
    //   String dbPath = join(databasesPath, 'my.db');
      
    //   var externalDir = await getExternalStorageDirectory();
    //   String path = join(externalDir.path, "database.db");

    // // Only copy if the database doesn't exist
    // //if (FileSystemEntity.typeSync(path) == FileSystemEntityType.notFound){
    //   // Load database from asset and copy
    //   ByteData data = await rootBundle.load(dbPath);
    //   List<int> bytes = data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);

    //   // Save copied asset to documents
    //   await new File(path).writeAsBytes(bytes);
    // //}

     
  // Future _files() async {
  //   var root = await getExternalStorageDirectory();
  //   var files = await FileManager(root: root).walk().toList();
  //   return files;
  // }
  // Future _getSpecificFileTypes() async {
  //   var root = await getExternalStorageDirectory();
  //   var files = await FileManager(root: root)
  //       .filesTree(extensions: ["txt", "3gp", "zip", "png"]);
  //   return files;
  // }

  // Future<void> writeToFile(ByteData data, String path) {
  //   final buffer = data.buffer;
  //   return new File(path).writeAsBytes(
  //       buffer.asUint8List(data.offsetInBytes, data.lengthInBytes));
  // }