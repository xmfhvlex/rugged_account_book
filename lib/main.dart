//import 'dart:io';
//import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_test_00/account_page.dart';
import 'package:flutter_test_00/option_page.dart';
import 'package:flutter/services.dart';
import 'user_data.dart';
import 'user/google_implement.dart';

//import 'package:googleapis/drive/v3.dart';


//  var client = GoogleHttpClient(await _googleSignIn.currentUser.authHeaders);
//   var api = DriveApi(client);

// Future uploadFile(DriveApi api, io.File file, String filename) {
//   var media = Media(file.openRead(), file.lengthSync());
//   return api.files.create(File.fromJson({"name": filename}), uploadMedia: media)
//     .then((File f) {
//         print('Uploaded $file. Id: ${f.id}');
//       }
//     ).whenComplete(() {
//     // reload content after upload the file
//     //_handleGetFiles();
//   });
// }


void main() async{

  var pages = { 
    "/":        (context) => MyHomePage(),
    "/sign_in": (context) => GoogleSignApp(),
    "/option":  (context) => OptionPage(),
  };

//  SystemUiOverlay.values
//  SystemChrome.setEnabledSystemUIOverlays([]);// StatusBar 숨기기
  await UserData.inst().initDatabase();

  runApp(
    MaterialApp(
      title: '엉성한 가계부',
      debugShowMaterialGrid: false,
      debugShowCheckedModeBanner: true,
      theme: ThemeData(
        brightness: Brightness.dark,
    //  backgroundColor: Color.fromARGB(255, 0, 0, 0),
        scaffoldBackgroundColor: Color.fromARGB(255, 45, 45, 45),
      ),
     // home: MyHomePage(title: '엉성한 가계부'),
      initialRoute: "/",
      routes: pages,
    )
  );
}














// import 'dart:async';
// import 'package:flutter/material.dart';

// import 'package:firebase_auth/firebase_auth.dart';
// import 'package:google_sign_in/google_sign_in.dart';

// void main() {
//   runApp(
//     new MaterialApp(
//       title: 'Google Sign In',
//       home: new LoginPage(),
//     ),
//   );
// }

// class LoginPage extends StatefulWidget {
//   @override
//   _LoginPageState createState() => _LoginPageState();
// }

// class _LoginPageState extends State<LoginPage> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: Container(
//         color: Colors.white,
//         child: Center(
//           child: Column(
//             mainAxisSize: MainAxisSize.max,
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: <Widget>[
//               FlutterLogo(size: 150),
//               SizedBox(height: 50),
//               _signInButton(),
//             ],
//           ),
//         ),
//       ),
//     );
//   }

//   Widget _signInButton() {
//     return OutlineButton(
//       splashColor: Colors.grey,
//       onPressed: () async { 
//         signInWithGoogle().whenComplete(() {
//           Navigator.of(context).push(
//             MaterialPageRoute(
//               builder: (context) {
//                 return FirstScreen();
//               },
//             ),
//           );
//         });
//       },
//       shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)),
//       highlightElevation: 0,
//       borderSide: BorderSide(color: Colors.grey),
//       child: Padding(
//         padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
//         child: Row(
//           mainAxisSize: MainAxisSize.min,
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: <Widget>[
//             Image(image: AssetImage("assets/google_logo.png"), height: 35.0),
//             Padding(
//               padding: const EdgeInsets.only(left: 10),
//               child: Text(
//                 'Sign in with Google',
//                 style: TextStyle(
//                   fontSize: 20,
//                   color: Colors.grey,
//                 ),
//               ),
//             )
//           ],
//         ),
//       ),
//     );
//   }
// }

// class FirstScreen extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: Container(color: Colors.blue[100]),
//     );
//   }
// }

// final FirebaseAuth _auth = FirebaseAuth.instance;
// final GoogleSignIn googleSignIn = GoogleSignIn();

// Future<String> signInWithGoogle() async {
//   GoogleSignInAccount googleUser = await googleSignIn.signIn();
//   GoogleSignInAuthentication googleAuth = await googleUser.authentication;
//   final AuthCredential credential = GoogleAuthProvider.getCredential(idToken: googleAuth.idToken, accessToken: googleAuth.accessToken);
//   var authResult = await _auth.signInWithCredential(credential);
//   final FirebaseUser user = authResult.user;

//   // final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
//   // final GoogleSignInAuthentication googleSignInAuthentication = await googleSignInAccount.authentication;

//   // final AuthCredential credential = GoogleAuthProvider.getCredential(
//   //   accessToken: googleSignInAuthentication.accessToken,
//   //   idToken: googleSignInAuthentication.idToken,
//   // );

//   // var authResult = await _auth.signInWithCredential(credential);
//   // final FirebaseUser user = authResult.user;

//   assert(!user.isAnonymous);
//   assert(await user.getIdToken() != null);

//   final FirebaseUser currentUser = await _auth.currentUser();
//   assert(user.uid == currentUser.uid);

//   return 'signInWithGoogle succeeded: $user';
// }

// void signOutGoogle() async{
//   await googleSignIn.signOut();

//   print("User Sign Out");
// }









// import 'dart:async';
// import 'package:flutter/material.dart';

// import 'package:firebase_auth/firebase_auth.dart';
// import 'package:google_sign_in/google_sign_in.dart';

// final FirebaseAuth _auth = FirebaseAuth.instance;
// final GoogleSignIn googleSignIn= new GoogleSignIn();

// GoogleSignIn _googleSignIn = new GoogleSignIn(
//   scopes: <String>[
//     'email',
//     'https://www.googleapis.com/auth/contacts.readonly',
//   ],
// );

// void main() {
//   runApp(
//     new MaterialApp(
//       title: 'Google Sign In',
//       home: new SignInDemo(),
//     ),
//   );
// }

// class SignInDemo extends StatefulWidget {
//   @override
//   _SignInDemoState createState() => new _SignInDemoState();
// }

// class _SignInDemoState extends State<SignInDemo> {

//    String _user_text = null;

//   Future<String> _testSignInAnonymously() async {
//     final FirebaseUser user = await _auth.signInAnonymously();
//     assert(user != null);
//     assert(user.isAnonymous);
//     assert(!user.isEmailVerified);
//     assert(await user.getIdToken() != null);

//     assert(user.providerData.length == 1);
//     assert(user.providerData[0].providerId == 'firebase');
//     assert(user.providerData[0].uid != null);
//     assert(user.providerData[0].displayName == null);
//     assert(user.providerData[0].photoUrl == null);
//     assert(user.providerData[0].email == null);
  
//     final FirebaseUser currentUser = await _auth.currentUser();
//     assert(user.uid == currentUser.uid);
//     print(currentUser.uid);
    
    
//     setState(() { _user_text=currentUser.uid; });


//     return 'signInAnonymously succeeded: $user';
//   }

//   Future<String> _testSignInWithGoogle() async {
//     final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
//     final GoogleSignInAuthentication googleAuth =
//         await googleUser.authentication;
//     final FirebaseUser user = await _auth.signInWithGoogle(
//       accessToken: googleAuth.accessToken,
//       idToken: googleAuth.idToken,
//     );
//     assert(user.email != null);
//     assert(user.displayName != null);
//     assert(!user.isAnonymous);
//     assert(await user.getIdToken() != null);

//     final FirebaseUser currentUser = await _auth.currentUser();
//     assert(user.uid == currentUser.uid);
//     print(currentUser.uid);
//     setState(() { _user_text=currentUser.uid; });
//     return 'signInWithGoogle succeeded: $user';
//   }

//     Future<Null> _handleSignOut() async {
//     await FirebaseAuth.channel.invokeMethod("signOut");
//     final FirebaseUser currentUser = await _auth.currentUser();
//     setState(() { _user_text=currentUser.uid; });
//   }

//   Future<Null> get_uid() async{
//      final FirebaseUser currentUser = await _auth.currentUser();
//     print(currentUser);
//     setState(() { _user_text=currentUser.uid; });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return new Scaffold(
//       body:   new Container(
//         child: new Center(
//           child: new Column(
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: <Widget>[
//             new Row(
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: <Widget>[
//               new RaisedButton(
//                 child: new Text("Sign Google "),
//                 color: Colors.redAccent,
//                 onPressed:  _testSignInWithGoogle,
//               ),
//               new RaisedButton(
//                 child: new Text("Sign Anonymously"),
//                 color: Colors.blueAccent,
//                  onPressed:_testSignInAnonymously,
//               ),
//               new RaisedButton(
//                 child: new Text("SignOut"),
//                 color: Colors.orangeAccent,
//                 onPressed: _handleSignOut,
//               )
//             ],
//           ),
//            _user_text == null ? new Text("Null"):new Text(_user_text),
//             ],
//           )

//         ),      
//       ),
//     );
//   }
// }
